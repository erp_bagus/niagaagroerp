﻿Imports System.Runtime.CompilerServices
Imports System.Globalization

Namespace HtmlHelpers
#Region "HelpersToolsDeclare"
    Public Module Helpers
        <Extension()> _
        Public Function LoadingHelpers(ByVal _helpers As HtmlHelper, ByVal url As String) As LoadingHelpers
            Return New LoadingHelpers(_helpers, url)
        End Function
        <System.Runtime.CompilerServices.Extension()> _
        Public Function WriteDateValue(ByVal helper As HtmlHelper, ByVal datevalue As Date?,
                                       Optional ByVal format As String = "dd-MMMM-yyyy", Optional ByVal nulldata As String = "") As HtmlString
            If datevalue.HasValue AndAlso datevalue.Equals(Date.MinValue) = False Then
                Dim ci As CultureInfo = New CultureInfo("id-ID")

                Return New HtmlString(datevalue.Value.ToString(format, ci))
            Else
                Return New HtmlString(nulldata)
            End If
            Return New HtmlString(String.Empty)
        End Function
    End Module
#End Region
#Region "HtmlHelpersClass"
    Public Class LoadingHelpers
        Implements IDisposable
        Protected _helpers As HtmlHelper
        Public Sub New(ByVal helpers As HtmlHelper, ByVal url As String)
            _helpers = helpers
            Dim Writer = helpers.ViewContext.Writer
            Writer.Write("<div class='modal fade' id='loadingmodal' tabindex='-1' role='dialog' arial-hidden = 'true'> " & vbCrLf)
            Writer.Write("    <div class='modal-dialog'>" & vbCrLf)
            Writer.Write("        <div class='modal-content'>" & vbCrLf)
            Writer.Write("            <div class='modal-body'>" & vbCrLf)
            Writer.Write("            <center>" & vbCrLf)
            Writer.Write("              <img src='" & url & "' alt='imgloading' />" & vbCrLf)
        End Sub
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dim Writer = _helpers.ViewContext.Writer
            Writer.Write("      </center>" & vbCrLf)
            Writer.Write("      </div>" & vbCrLf)
            Writer.Write("  </div>" & vbCrLf)
            Writer.Write(" </div>" & vbCrLf)
            Writer.Write("</div>" & vbCrLf)
        End Sub
    End Class
#End Region

End Namespace
