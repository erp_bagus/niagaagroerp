﻿var tblType;
$(document).ready(function () {
    $('.btn-add').click(function () {
        fnReset('form-type');
        fnRemoveField('#form-type');
    });
    var button = "<button class='btn btn-primary edit-data' rel='tooltip' title='Edit' data-placement='left'>" +
                  "<i class='glyphicon glyphicon-pencil'></i></button>" +
                " <button class='btn btn-danger btn-del' rel='tooltip' title='Delete' data-placement='right'>" +
                "<i class='glyphicon glyphicon-remove'></i></button>";
    //Tabel SoW Datatable
    tblType = $('#tbltype').DataTable({
        "ajax": "/Type/Details",
        "columns": [
        { "data": "Type" },
        { "defaultContent": button }
        ],
        "fnDrawCallback": function () {
            $("[rel='tooltip']").tooltip();
        }
    });

    var Ttype = $('#tbltype tbody');
    Ttype.on('click', '.edit-data', function () {
        fnRemoveField('#form-type');
        var data = tblType.row($(this).parents('tr')).data();
        $('#Type').val(data.Type);
        $('<input type="hidden" name="ID" id="ID" value="' + data.ID + '"/>').appendTo('#form-type');
        $('#modaltype').modal('show');
    })
    .on('click', '.btn-del', function () {
        var data = tblType.row($(this).parents('tr')).data();
        var urlDelete = '/Type/Delete';
        fnDelete(data.ID, urlDelete, tblType, '#loadingmodal');
    });
    $("[rel='tooltip']").tooltip();

    $('#form-type').submit(function (e) {
        e.preventDefault();
        fnInsert($(this), '#modaltype', tblType);
    });
});