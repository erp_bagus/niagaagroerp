﻿var tblSale;
$(document).ready(function () {
   var button = "<a href='javascript:return edit("data":"ID")' class='btn btn-primary edit-data' rel='tooltip' title='Edit' data-placement='left'>" +
                "<i class='glyphicon glyphicon-pencil'></i></a>" +
                "<button class='btn btn-danger btn-del' rel='tooltip' title='Delete' data-placement='right'>" +
                "<i class='glyphicon glyphicon-remove'></i></button>";
    //Tabel SoW Datatable
    tblSale = $('#tblsale').DataTable({
        "ajax": "/Sale/Details",
        "columns": [
        { "data": "FormNumber" },
        { "data": "Nama" },
        { "data": "Tanggal" },
        { "data": "Type" },
        { "defaultContent": button }
        ],
        "columnDefs": [
        {
            "render": function (data, type, row) {
                return fnshortdate(data)
            },
            "targets": 2
        }
        ],
        "fnDrawCallback": function () {
            $("[rel='tooltip']").tooltip();
        }
    });

    var tSale = $('#tblsale tbody');
    tSale.on('click', '.edit-data', function () {
        fnRemoveField('#formsale');
        var data = tblSale.row($(this).parents('tr')).data();
        $('#IDBarang').val(data.IDBarang);
        var tgl = fnshortdate(tgl);
        $('#Tanggal').val(tgl);
        $('#Jumlah').val(data.Jumlah);
        $('#Type').val(data.Type);
        $('#HargaJual').val(data.HargaJual);
        $('#Keterangan').val(data.Keterangan);
        $('<input type="text" name="ID" id="ID" value="' + data.ID + '"/>').appendTo('#formsale');
        $('#modalsale').modal('show');
    })
    .on('click', '.btn-del', function () {
        var data = tblSale.row($(this).parents('tr')).data();
        var urlDelete = '/Sale/Delete';
        fnDelete(data.ID, urlDelete, tblSale, '#loadingmodal');
    });
    $("[rel='tooltip']").tooltip();

});



//"<button class='btn btn-primary edit-data' rel='tooltip' title='Edit' data-placement='left'>" +
//                  "<i class='glyphicon glyphicon-pencil'></i></button>"