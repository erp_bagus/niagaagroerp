﻿$(document).ready(function () {
    $("[rel='tooltip']").tooltip();
    //autonumeric
    $('.auto').each(function () {
        fnautonumeric($(this));
    });
    $(document).on('focus', '.auto', function () {
        fnautonumeric($(this));
    });
    //
    $('#TotalBelanja').autoNumeric('set', 0);
    fnSelect('#IDBarang');
    fndatepicker('#Tanggal');

    //tambah2 tabel untuk mengisi data
    //menghapus tabel yg telah ditambah
    $(document).on('click', '.btn-remove', function () {
        $(this).closest('table').remove();
    });
    //menambah tabel
    $(document).on('click', '.btn-add', function () {
        var clonetbl;
        clonetbl = $('#cloning').clone();
        $(clonetbl.html()).insertBefore('.form-horizontal');
    });
    //
    $('#Type').change(function () {
        var value = $(this).val();
        if (value == "5") {
            $('#dp').show();

        } else {
            $('#dp').hide();
        }
    });

    var totalHarga = $('#TotalBelanja').autoNumeric('get');
    $(document).on('change', '.harga', function () {
        
        $(this).each(function () {
            var total = $(this).autoNumeric('get');
            total = parseInt(total);
            totalHarga = parseInt(totalHarga);
            totalHarga= totalHarga +total;
        });
        $('#TotalBelanja').autoNumeric('set', totalHarga);
    });


    var idBarangValidators = {
        row: '.col-xs-2',   // The title is placed inside a <div class="col-xs-4"> element
        validators: {
            notEmpty: {
                message: 'The barang is required'
            }
        }
    },
        jumlahValidators = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'The jumlah is required'
                },
                numeric: {
                    message: 'The jumlah is not valid'
                }
            }
        },
        hargaValidators = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'The harga is required'
                },
                numeric: {
                    message: 'The harga must be a numeric number'
                }
            }
        },
        barangIndex = 0;

      $("#formsale").formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                '[0].IDBarang': idBarangValidators,
                '[0].Jumlah': jumlahValidators,
                '[0].HargaJual': hargaValidators
            }
        })

    // Add button click handler
        .on('click', '.addButton', function () {
            barangIndex++;
            var $template = $('#barangTemplate'),
                $clone = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-index', barangIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('select[name="IDBarang"]').attr('name', '[' + barangIndex + '].IDBarang').end()
                .find('[name="Jumlah"]').attr('name', '[' + barangIndex + '].Jumlah').end()
                .find('[name="HargaJual"]').attr('name', '[' + barangIndex + '].HargaJual').end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#formsale')
                .formValidation('addField', '[' + barangIndex + '].IDBarang', idBarangValidators)
                .formValidation('addField', '[' + barangIndex + '].Jumlah', jumlahValidators)
                .formValidation('addField', '[' + barangIndex + '].HargaJual', hargaValidators);
        })

    // Remove button click handler
        .on('click', '.removeButton', function () {
            var $row = $(this).parents('.form-group'),
                index = $row.attr('data-index');

            // Remove fields
            $('#formsale')
                .formValidation('removeField', $row.find('select[name="[' + index + '].IDBarang"]'))
                .formValidation('removeField', $row.find('[name="[' + index + '].Jumlah"]'))
                .formValidation('removeField', $row.find('[name="[' + index + '].HargaJual"]'));

            // Remove element containing the fields
            $row.remove();
        });
});