﻿$(document).ready(function () {
    $("#IDCustomer").addClass("form-control");
    $("#Type").addClass("form-control");
    $("#Type").attr("id", "type");
    $("#tblsale").DataTable();
    ($('#type').val() != '5') ? $('#duedate').hide().attr('disabled', true) : $('#duedate').show().attr('disabled', false);
    $(".duedate").datetimepicker({ format: "DD-MM-YYYY" });
    $(".tgllunas").datetimepicker({ format: "DD-MM-YYYY" });
    $('.dtpaid').autoNumeric('init', { aSep: '.', aDec: ',' });
    $('.temptotalharga').autoNumeric('init', { aSep: '.', aDec: ',' });
    $('#type').change(function () {
        if ($('#type').val() == '5') {
            $('#duedate').show().attr('disabled', false);
            $('.duedate').show().attr('disabled', false);
        } else {
            $('#duedate').hide().attr('disabled', true);
            $('.duedate').hide().attr('disabled', true);
        }
    });

    listbarang();
    KeyNumber(0);
    var sum = 0;
    $('.tempharga').autoNumeric('init', { aSep: '.', aDec: ',' });
    $(".jumlah").change(function () {
        Total();
    });
    $(".tempharga").change(function () {
        var parent = $(this).parent().parent();
        val = $(this).autoNumeric('get');

        //add only if the value is number
        if (!isNaN(val) && val.length != 0) {
            sum += parseFloat(val);
            //$(this).val(accounting.formatMoney(val, "", 2, ".", ","));
        }
        Total();
    });
    $(".tempbayar").change(function () {
        var buy = $(this).autoNumeric('get');
        $(".bayar").val(parseFloat(buy));

    });
    Total();
    $("#Bayar").keydown(function (e) {
        onlyNumber(e);
    });
    SetIndex();
    var index = $("#_index").val();
    $("#AddItem").click(function () {
        var strHTML = "";
        var data = JSON.parse($('.tempidbarang').val());
        if (data != null) {
            strHTML += "<option value='0'>--Select Barang--</option>";
            for (var i = 0; i < data.length; i++) {
                strHTML += "<option value='" + data[i].Id + "'>" + data[i].Name + "</option>";
            }
        }
        var Html = "";
        Html += "<tr id='" + index + "'>";
        Html += "<td class='td-no'><span> + index + </span><input type='hidden' id='ID_" + index + "' name='[" + index + "].ID' value='' /><input type='hidden' id='Tanggal_" + index + "' name='[" + index + "].Tanggal' value=''></td>";
        Html += "<td class='td-idbarang'><select id='IDBarang_" + index + "' name='[" + index + "].IDBarang' class='form-control idbarang' style='width:100%;'></select></td>";
        Html += "<td class='td-jumlah' style='width:30px;'><input type='text' id='Jumlah_" + index + "' name='[" + index + "].Jumlah' style='width:100%;' class='form-control jumlah'></td>";
        Html += "<td class='td-hargajual'><input type='text' name='[" + index + "].TempHarga' id='TempHarga_" + index + "' style='width:100%;' class='form-control tempharga' /><input type='hidden' name='[" + index + "].HargaJual' id='HargaJual_" + index + "' style='width:100%;' class='form-control harga'></td>";
        Html += "<td class='td-keterangan'><input type='text' name='[" + index + "].Keterangan' id='Keterangan_" + index + "' style='width:100%;' class='form-control'></td>";
        Html += "<td class='td-action'><a data-id = '" + (Number(index)) + "' onclick='removerow(this);' class='glyphicon glyphicon-remove'  id='move' title='batal'></a></td>";
        Html += "</tr>";
        $("#List").append(Html);

        $("#IDBarang_" + index).append(strHTML);
        KeyNumber(index);
        var sum = 0;
        $('.tempharga').autoNumeric('init', { aSep: '.', aDec: ',' });
        $(".tempharga").change(function () {
            var parent = $(this).parent().parent();
            val = $(this).autoNumeric('get');
            //add only if the value is number
            if (!isNaN(val) && val.length != 0) {
                sum += parseFloat(val);
                //$(this).val(accounting.formatMoney(val, "", 2, ".", ","));
            }
            Total();
        });
        $(".jumlah").change(function () {
            Total();
        });
        index++;
        SetIndex();

    });


});

onlyNumber = function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
    // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
    // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
    // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }

}

function KeyNumber(no) {
    $("#Jumlah_" + no).keydown(function (e) {
        onlyNumber(e);
    });

    $("#TempHarga_" + no).keydown(function (e) {
        onlyNumber(e);
    });

}

function listbarang() {
    $.getJSON('/Sale/GetNamaBarang/', function (data) {
        $('.tempidbarang').val(JSON.stringify(data));
    });
}

removerow = function (a) {
    hasil = 0;
    //console.log(c);
    tr = $(a).parent().parent();
    $(tr).remove();
    c = $(a).attr("id");
    Total(c);
    SetIndex();

    var buy = accounting.unformat($('.bayar').val(), ",");
    var total = accounting.unformat($('.total').val(), ",");
    console.log(total);
    if (buy != "") {
        var bayar = buy - total;
        $("#Kembalian").val(accounting.formatMoney(bayar, "Rp. ", 2, ".", ","));
    }

}

Total = function (c) {
    var nilai = 0;
    var result = 0;
    var jumlah = 0;
    var arrJumlah = [];
    $('#barang .jumlah').each(function () {
        arrJumlah.push($(this).val());
        $("#tempArrJumlah").val(arrJumlah.join(","));
    });
    $('.tempharga').each(function (index, item) {
        var num = $(item).autoNumeric('get');
        if (!isNaN(num) && num.length != 0) {
            var arrJum = $("#tempArrJumlah").val().split(",");
            nilai += parseFloat(num * arrJum[index]);
            //console.log(nilai);
        }
        $('#HargaJual_' + index).val(num);
    });

    $('.totalbelanja').html(accounting.formatMoney(nilai, "Rp. ", 2, ".", ","));
    $('.total').val(accounting.formatMoney(nilai, "Rp. ", 2, ".", ","));
    var total = accounting.unformat($('.total').val(), ",");
    $('.temptotalbelanja').val(total);
    //console.log(total);
    if (total != 0) {
        $('.tempbayar').autoNumeric('init', { aSep: '.', aDec: ',' });
        $(".tempbayar").change(function () {
            var buy = $(this).autoNumeric('get');
            // console.log(buy);
            if (buy != "") {
                var bayar = buy - total;
                $("#Kembalian").val(accounting.formatMoney(bayar, "Rp. ", 2, ".", ","));
            } else {
                $("#Kembalian").val("");

            }

        });
    } else {
        $('.total').val("");
        $(".tempbayar").val("");
        $("#Kembalian").val("");
        $(".bayar").val("");
    }

    var bayar = accounting.unformat($('.tempbayar').val(), ",");
    if (bayar != "" && total != "") {
        var bayar = bayar - total;
        $("#Kembalian").val(accounting.formatMoney(bayar, "Rp. ", 2, ".", ","));

    }

    //---paid transaction--------------------

    var paid = 0;
    var sisa = 0;
    var maxbayar;
    var subnilai = 0;
    $('.totalharga').each(function (index, item) {
        var num = parseFloat($(this).val().replace(',0000'));
        if (!isNaN(num) && num.length != 0) {
            subnilai += num;
        }
        $('#TotalHarga_' + index).val(num);
    });
    $('.jumlahhargabelanja').html(accounting.formatMoney(subnilai, "Rp. ", 2, ".", ","));
    jumlahhargabelanja = accounting.unformat($('.jumlahhargabelanja').html(), ",");
    $("#jumlahhargabelanja").val(jumlahhargabelanja);
    $('.dtpaid').change(function () {
        paid = $(this).autoNumeric('get');
        tempbayar = accounting.unformat($('.tempbayar').html(), ",");
        maxbayar = $('#maxbayar').val();
        console.log(maxbayar);
        $('.paid').val(paid);
        if (maxbayar == 0) {
            if (jumlahhargabelanja != 0) {
                sisa = (jumlahhargabelanja - tempbayar - paid);
                $('.sisa').val(sisa);
                $('.dtsisa').val(accounting.formatMoney(sisa, "Rp. ", 2, ".", ","));
            }
        } else {
            if (jumlahhargabelanja != 0) {
                var repmaxbayar = parseFloat(maxbayar.replace(",0000"));
                //console.log(repmaxbayar);
                sisa = (jumlahhargabelanja - tempbayar - repmaxbayar - paid);
                $('.sisa').val(sisa);
                $('.dtsisa').val(accounting.formatMoney(sisa, "Rp. ", 2, ".", ","));
            }
        }
    });

}

SetIndex = function () {
    $("#List tr").each(function (idx) {
        var tr = $(this);
        $(tr).attr("id", idx);
        $(tr).find("td.td-no span").html(idx + 1);
        $(tr).find("td.td-no input:eq(0)").attr("id", "ID_" + idx).attr("name", "[" + idx + "].ID");
        $(tr).find("td.td-no input:eq(1)").attr("id", "Tanggal_" + idx).attr("name", "[" + idx + "].Tanggal");
        $(tr).find("td.td-idbarang select:eq(0)").attr("id", "IDBarang_" + idx).attr("name", "[" + idx + "].IDBarang");
        $(tr).find("td.td-idbarang option:eq(1)").attr("id", "IDBarang_" + idx).attr("name", "[" + idx + "].IDBarang");
        $(tr).find("td.td-jumlah input:eq(0)").attr("id", "Jumlah_" + idx).attr("name", "[" + idx + "].Jumlah");
        $(tr).find("td.td-hargajual input:eq(0)").attr("id", "TempHarga_" + idx).attr("name", "[" + idx + "].TempHarga");
        $(tr).find("td.td-hargajual input:eq(1)").attr("id", "HargaJual_" + idx).attr("name", "[" + idx + "].HargaJual");
        $(tr).find("td.td-keterangan input:eq(0)").attr("id", "Keterangan_" + idx).attr("name", "[" + idx + "].Keterangan");
        //console.log($(tr).find("td.td-no input:eq(0)"));
    });

    save = function () {
        if (confirm("Apakah data akan disimpan?")) {
            submitForm();
            return false;
        }
    }

    submitForm = function () {
        $("#alertplaceholder").empty();
        var obj = $("#formsale");
        var data = $(obj).serialize();
        var url = $(obj).attr("action");
        //console.log(url);
        $.ajax({
            type: 'POST',
            url: url,
            data: data, //function (data) { alert(data)},
            success: submitFormCallback,
            error: function (data) {
                console.log(data.stat)
                alert(data)
            },
            dataType: 'json'
        });

        return false;
    }

    submitFormCallback = function (data) {
        if (data.stat == 0) {
            $("#btnGo").toggle();
            $("#btnOk").toggle();
            $("#alertplaceholder").append("<div class='alert'><h2 style='margin-top:0'>Data Input</h2><ul></ul></div>");
            for (var i = 0; i < data.message.length; i++) {
                $("#alertplaceholder ul").append("<li>" + data.message[i].Value + "</li>");
            }
            $("#dgModal").modal();
            $("#btnGo").click(function () {
                console.log(data);
                $("#dgModal").modal('hide');
            });
        } else {
            window.location.href = "/Sale/Index";
        }
    }
}

delpenjualan = function (Id) {
    var result = confirm("Apakah Anda yakin untuk menghapus data ini?");
    console.log(result);
    if (result != true) {

        return false;
    }
    else {
        $.ajax({
            type: 'POST',
            url: '/Sale/DeletePenjualan',
            data: { Id: Id },
            success: function (data) {
                window.location.href = "/Sale/Index";
            },
            error: ajax_error_callback,
            dataType: 'json'
        });
    }
}

ajax_error_callback = function (xhr, status, data) {
    showIndicator(false);   //if shown
    alert(xhr.status + ':' + xhr.statusText)
}

//------TODO js Edit data penjualan


    //$(".edit-data").click(function (id){
editsale = function (id) {
    $("#0").remove();
    $.getJSON('/Sale/GetPenjualanData', { id: id }, function (data) {
        $("#ID").val(data.ID);
        $("#FormNumber").val(data.FormNumber);
        $("#Tanggal").val(fnshortdate(data.Tanggal));
        $.get("/Sale/GetIDCustomer/" + data.IDCustomer, function (data) {
            $('option[value="' + data.ID + '"]', $('#IDCustomer')).prop('selected', true);
        });
        $.get("/Sale/GetIDType/" + data.Type, function (data) {
            $('option[value="' + data.ID + '"]', $('#Type')).prop('selected', true);
        });
    });

    $.getJSON('/Sale/GetPenjualanDetailData', { id: id }, function (model) {
        //alert(model.length);
        if (model.length > 0) {
            var no = 0;
            var html1 = "";
            var strHTML = "";
            var tempid = JSON.parse($('.tempidbarang').val());
            if (tempid != null) {
                strHTML += "<option value='0'>--Select Barang--</option>";
                for (var j = 0; j < tempid.length; j++) {
                    strHTML += "<option value='" + tempid[j].Id + "'>" + tempid[j].Name + "</option>";
                }
            }
            for (var i = 0; i < model.length; i++) {
                html1 += "<tr id='" + no + "' value='" + no + "'>";
                html1 += "   <td class='td-no'><input type='hidden' id='ID_" + no + "' name='[" + no + "].ID' value='" + model[i].ID + "' /><input type='hidden' id='Tanggal_" + no + "' name='[" + no + "].Tanggal' value='" + model[i].Tanggal + "'></td>";
                html1 += "   <td class='td-idbarang'><select id='IDBarang_" + no + "' name='[" + no + "].IDBarang' class='form-control idbarang' style='width:100%;' data-id = '" + no + "'></select></td>";
                html1 += "   <td class='td-jumlah'><input type='text' id='Jumlah_" + no + "' name='[" + no + "].Jumlah' style='width:100%;' value='" + model[i].Jumlah + "' class='form-control jumlah'></td>";
                html1 += "   <td class='td-hargajual'><input type='text' name='[" + no + "].TempHarga' id='TempHarga_" + no + "' style='width:100%;' value='" + model[i].HargaJual + "' class='form-control tempharga' /><input type='hidden' name='[" + no + "].HargaJual' id='HargaJual_" + no + "' style='width:100%;'  value='" + model[i].HargaJual + "' class='form-control harga'></td>";
                html1 += "   <td class='td-keterangan'><input type='text' name='[" + no + "].Keterangan' id='Keterangan_" + no + "' style='width:100%;' value='" + model[i].Keterangan + "' class='form-control'></td>";
                html1 += "   <td class='td-action'><a data-id = '" + no + "' onclick='removerow(this);' class='glyphicon glyphicon-remove' title='batal' id='move'></a></td>";
                html1 += " </tr>";
                $("#List").append(html1);
                console.log(no);
                if (no == 0) { $('#move').removeAttr('class'); }
                $("#IDBarang_" + no).append(strHTML);
                //selectedBarang(model[i].IDBarang, $('#').attr(id));
                KeyNumber(no);
                var sum = 0;
                $('.tempharga').autoNumeric('init', { aSep: '.', aDec: ',' });
                $(".tempharga").change(function () {
                    var parent = $(this).parent().parent();
                    val = $(this).autoNumeric('get');
                    //add only if the value is number
                    if (!isNaN(val) && val.length != 0) {
                        sum += parseFloat(val);
                    }
                    Total();
                });
                var idx = $("#" + no).html();
                console.log(idx);
                $(idx).remove();
                no++;
                //SetIndex();
            }
            //$("#1").remove();
        }
    });

}

selectedBarang = function (e,no) {
    $.get("/Sale/GetIdBarang/" + e, function (data) {
        console.log(no);
            $('option[value="' + data.Id + '"]', $('#IDBarang_' + no)).prop('selected', true);
    });

}