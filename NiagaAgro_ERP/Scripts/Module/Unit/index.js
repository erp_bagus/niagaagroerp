﻿var tblUnits;
$(document).ready(function () {
    $('.btn-add').click(function () {
        fnRemoveField('#form-unit');
        fnReset('form-unit');
    });
    var button = "<button class='btn btn-primary edit-data' rel='tooltip' title='Edit Ticket' data-placement='left'>" +
                  "<i class='glyphicon glyphicon-pencil'></i></button>" +
                " <button class='btn btn-danger btn-del' rel='tooltip' title='Delete' data-placement='right'>" +
                "<i class='glyphicon glyphicon-remove'></i></button>";
    //Tabel SoW Datatable
    tblUnits = $('#tblunit').DataTable({
        "ajax": "/Unit/Details",
        "columns": [
        { "data": "Satuan" },
        { "data": "Keterangan" },
        { "defaultContent": button }
        ],
        "fnDrawCallback": function () {
            $("[rel='tooltip']").tooltip();
        }
    });
    var tunit = $('#tblunit tbody');
    tunit.on('click', '.edit-data', function () {
        fnRemoveField('#form-unit');
        var data = tblUnits.row($(this).parents('tr')).data();
        $('#Satuan').val(data.Satuan);
        $('#Keterangan').val(data.Keterangan);
        $('<input type="hidden" name="ID" id="ID" value="' + data.ID + '"/>').appendTo('#form-unit');
        $('#modalunit').modal('show');
    })
    .on('click', '.btn-del', function () {
        var data = tblUnits.row($(this).parents('tr')).data();
        var urlDelete = '/Unit/Delete';
        fnDelete(data.ID, urlDelete, tblUnits, '#loadingmodal');
    });
    $("[rel='tooltip']").tooltip();
//    $('#form-unit').submit(function (e) {
//        e.preventDefault();
//        fnInsert($(this), '#modalunit', tblUnits);
//    });
});