﻿var tblPurchase;
$(document).ready(function () {
    $("#Tanggal,#TanggalPO").datetimepicker({ format: "DD-MM-YYYY" });
    fnautonumeric('#Modal');

    $('.btn-add').click(function () {
        fnRemoveField('#form-purchase');
        fnReset('form-purchase');
    });

    fnSelect('#IDBarang');
    fndatepicker('#Tanggal');
    fndatepicker('#TanggalPO');
    $(document).click(function () {
        var totalVal = $('#Modal').autoNumeric('get');
        $('#Modal').val(totalVal);
        $('#Modal').valid();
    });

    var button = "<button class='btn btn-primary edit-data' rel='tooltip' title='Edit' data-placement='left'>" +
                  "<i class='glyphicon glyphicon-pencil'></i></button>" +
                " <button class='btn btn-danger btn-del' rel='tooltip' title='Delete' data-placement='right'>" +
                "<i class='glyphicon glyphicon-remove'></i></button>";
    //Tabel SoW Datatable
    tblPurchase = $('#tblPurchase').DataTable({
        "ajax": "/Purchase/Details",
        "columns": [
        { "data": "NamaBrg" },
        { "data": "Tanggal" },
        { "data": "Jumlah" },
        { "data": "Modal" },
        { "data": "JenisBarang" },
        { "defaultContent": button }
        ],
        "columnDefs": [
        {
            "render": function (data, type, row) {
                return fnshortdate(data);
            },
            "targets": 1
        }
        ],
        "order": [[4, "desc"]],
        "fnDrawCallback": function () {
            $("[rel='tooltip']").tooltip();
        }
    });

    var tPurchase = $('#tblPurchase tbody');
    tPurchase.on('click', '.edit-data', function () {
        fnRemoveField('#form-purchase');
        var data = tblPurchase.row($(this).parents('tr')).data();
        $('#IDBarang').val(data.IDBarang);
        $('#Tanggal').val(fnshortdate(data.Tanggal));
        $('#Jumlah').val(data.Jumlah);
        $('#Distributor').val(data.Distributor);
        $('#NomorPO').val(data.NomorPO);
        $('#TanggalPO').val(fnshortdate(data.TanggalPO));
        $('#Modal').val(data.Modal);
        $('#Keterangan').val(data.Keterangan);
        $('<input type="hidden" name="ID" id="ID" value="' + data.ID + '"/>').appendTo('#form-purchase');
        $('#modalpurchase').modal('show');
    })
    .on('click', '.btn-del', function () {
        if (confirm("Apakah data akan dihapus?")) {
            var data = tblPurchase.row($(this).parents('tr')).data();
            var urlDelete = '/Purchase/Delete';
            fnDelete(data.ID, urlDelete, tblPurchase, '#loadingmodal');
        }
    });
    $("[rel='tooltip']").tooltip();
    //    $('#form-purchase').submit(function (e) {
    //        e.preventDefault();
    //        fnInsert($(this), '#modalpurchase', tblPurchase);
    //    });
});