﻿var tblmaterial;
$(document).ready(function () {
    $('.btn-add').click(function () {
        fnRemoveField('#form-material');
        fnReset('form-material');
    });

    var button = "<button class='btn btn-primary edit-data' rel='tooltip' title='Edit' data-placement='left'>" +
                  "<i class='glyphicon glyphicon-pencil'></i></button>" +
                " <button class='btn btn-danger btn-del' rel='tooltip' title='Delete' data-placement='right'>" +
                "<i class='glyphicon glyphicon-remove'></i></button>";
    //Tabel SoW Datatable
    tblmaterial = $('#tblbrg').DataTable({
        "ajax": "/Material/Details",
        "columns": [
        { "data": "NamaBrg" },
        { "data": "Kemasan" },
         { "data": "Satuan" },
         { "data": "JenisBarang" },
        { "defaultContent": button }
        ],
        "order": [[3, "desc"]],
        "fnDrawCallback": function () {
            $("[rel='tooltip']").tooltip();
        }
    });
    var tmaterial = $('#tblbrg tbody');
    tmaterial.on('click', '.edit-data', function () {
        fnRemoveField('#form-material');
        var data = tblmaterial.row($(this).parents('tr')).data();
        $('#NamaBrg').val(data.NamaBrg);
        $('#Kemasan').val(data.Kemasan);
        $('#IDUnit').val(data.IDUnit);
        $('<input type="hidden" name="ID" id="ID" value="' + data.ID + '"/>').appendTo('#form-unit');
        $('#modalmaterial').modal('show');
    })
    .on('click', '.btn-del', function () {
        if (confirm("Apakah data akan dihapus?")) {
            var data = tblmaterial.row($(this).parents('tr')).data();
            var urlDelete = '/Material/Delete';
            fnDelete(data.ID, urlDelete, tblmaterial, '#loadingmodal');
        }
    });
    $("[rel='tooltip']").tooltip();
       /* $('#form-material').submit(function (e) {
            e.preventDefault();
            fnInsert($(this), '#modalmaterial', tblmaterial);
        });*/
});