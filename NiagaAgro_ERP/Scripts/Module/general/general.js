﻿fnautonumeric = function (obj) {
    $(obj).autoNumeric('init', { aSep: '.', aDec: ',' });
}
fnInsert = function (idForm, idModalForm, GenTable) {
    $.ajax({
        type: 'POST',
        url: $(idForm).attr('action'),
        data: $(idForm).serialize(),
        dataType: 'json',
        beforeSend: function () {
            $(idModalForm).modal('hide');
            $('#loadingmodal').modal({
                backdrop: 'static'
            });
        },
        success: function (get) {
            if (get.stat == 1) {
                GenTable.ajax.reload();
            } else {
                alert(get.msg);
            }
            return false;
        },
        error: function (xhr, status, data) {
            alert("Error " + status.text + " ,Please Call Adminstrator");
        },
        complete: function () {
            
            $('#loadingmodal').modal('hide');
        }
    });
}
//reset
fnReset = function (obj) {
    document.getElementById(obj).reset();
}
//menghapus data sow master
fnDelete = function (ID, url, table, objModal) {
    $(objModal).modal({
        backdrop: 'static'
    });
    $.ajax({
        type: 'POST',
        url: url,
        data: { id: ID },
        dataType: 'json',
        success: function (get) {
            if (get.stat == 1) {
                table.ajax.reload();
            }
            return false;
        },
        error: function (xhr, status, data) {
            alert("Cannot Delete " + ID + " ,Please Call Adminstrator");
        },
        complete: function () {
            $(objModal).modal('toggle');
        }
    });
}
getUrlParameter = function (sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
fndatepicker = function (obj) {
    $(obj).datetimepicker({
        format: 'MM-DD-YYYY'
    });
}
fnshortdate = function (val) {
    return moment(val).format( "DD-MM-YYYY");
}
fnRemoveField = function (objform) {
    $(objform).find('#ID').remove();
}
fnSelect = function (obj) {
    var length = $(obj).children('option').length;
    if (length > 10) {
        $(obj).select2();
    }
}