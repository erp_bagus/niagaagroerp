﻿$(document).ready(function () {
    $("#tblUser").DataTable();

    Edit = function (username) {
        $("#UserName").val("");
        $("#NewPassword").val("");
        $("#ConfirmPassword").val("");
        $("#ConfirmNewPassword").val("");
        $("#OldPassword").val("");
        $("option:selected").removeAttr("selected");
        //$(".username").attr("disabled", "disabled");

        $.ajax({
            url: '/UserManagement/GetById/',
            type: 'POST',
            data: {
                username: username
            },
            success: function (result) {
                $(".username").attr("value", result.UserName);
                $("#name").html(result.UserName);
                $(".email").attr("value", result.Email);

            }
        });

        $.ajax({
            url: '/UserManagement/GetUserInRoles/',
            type: 'POST',
            data: {
                username: username
            },
            success: function (result) {
                console.log(result);
                $("#Roles option").filter(function () {
                    return $(this).attr("value") == result;
                }).attr("selected", "selected");

            }
        });

    };

    Save = function () {
        var obj = $("#formedituser");
        var data = $(obj).serialize();
        var url = $(obj).attr("action");
        $.ajax({
            type: 'POST',
            url: '/UserManagement/ChangePassword/',
            data: data,
            success: PostResponse,
            error: function (data) {
                console.log(data.stat);
                // alert(data.stat)
            },
            dataType: 'json'
        });

        return false;
    }

    PostResponse = function (data) {
        if (data.stat == 0) {
            $("#AlertUser").show("fast");
            var ValidationMessage = "";
            ValidationMessage += "<ul style='color:red;font-size:15px;font-weight:bold'>";
            for (var i = 0; i < data.err.length; i++) {
                ValidationMessage += data.err[i];
            }
            ValidationMessage += "</ul>";
            $("#AlertUserMessage").html(ValidationMessage);
        }
        else {
            reloadPage();
        }
    }

    var actionpath = '/UserManagement/';
    reloadPage = function (anchor) {
        window.location.href = actionpath;
        window.location.reload(true);
    }

    Delete = function (username) {
        var result = confirm("Are you sure to delete?");
        if (result != true) {
            return false;
        }
        else {
            $.ajax({
                url: "/UserManagement/Delete/",
                type: 'POST',
                data: {
                    username: username
                },
                success: function (result) {
                    alert("Succes, Data has been deleted!");
                    window.location.reload(true);

                }
            });

        }
    }

});