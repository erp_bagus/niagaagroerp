﻿var tblCustomer;
$(document).ready(function () {
    $("#JenisKelamin").addClass("form-control");
    $('.btn-add').click(function () {
        fnRemoveField('#form-customer');
        fnReset('form-customer');
    });

    var button = "<button class='btn btn-primary edit-data' rel='tooltip' title='Edit' data-placement='left'>" +
                  "<i class='glyphicon glyphicon-pencil'></i></button>" +
                " <button class='btn btn-danger btn-del' rel='tooltip' title='Delete' data-placement='right'>" +
                "<i class='glyphicon glyphicon-remove'></i></button>";
    //Tabel SoW Datatable
    tblCustomer = $('#tblCustomer').DataTable({
        "ajax": "/Customer/Details",
        "columns": [
        { "data": "Nama" },
        { "data": "JenisKelamin" },
        { "data": "Alamat" },
        { "data": "Phone" },
        { "defaultContent": button }
        ],
        "fnDrawCallback": function () {
            $("[rel='tooltip']").tooltip();
        }
    });

    var datacustomer = $('#tblCustomer tbody');
    datacustomer.on('click', '.edit-data', function () {
        fnRemoveField('#form-customer');
        var data = tblCustomer.row($(this).parents('tr')).data();
        $('#ID').val(data.ID);
        $('#Nama').val(data.Nama);
        $('#Alamat').val(data.Alamat);
        $('#Phone').val(data.Phone);
        $('#JenisKelamin').val(data.JenisKelamin);
        $('<input type="hidden" name="ID" id="ID" value="' + data.ID + '"/>').appendTo('#form-customer');
        $('#modalcustomer').modal('show');
    })
    .on('click', '.btn-del', function () {
        if (confirm("Apakah data akan dihapus?")) {
            var data = tblCustomer.row($(this).parents('tr')).data();
            var urlDelete = '/Customer/Delete';
            fnDelete(data.ID, urlDelete, tblCustomer, '#loadingmodal');
        }
    });
    $("[rel='tooltip']").tooltip();

});