﻿Namespace NiagaAgro_ERP
    Public Class PurchaseController
        Inherits System.Web.Mvc.Controller
        Dim ctx As New NiagaAgroEntities
        '
        ' GET: /Purchase
        <Authorize()>
        Function Index() As ActionResult
            ViewData("Barang") = (From b In ctx.MasterBarangs Select New With {b.ID, b.NamaBrg}).ToList
            Return View()
        End Function

        '
        ' GET: /Purchase/Details/5

        Function Details() As JsonResult
            Dim model = (From p In ctx.TblBrgMasuks Select New With {p.ID, p.IDBarang, p.MasterBarang.TblSatuanUnit.Satuan, p.Jumlah,
                                                                    p.Keterangan, p.Tanggal, p.Modal, p.Distributor, p.NomorPO,
                                                                     .NamaBrg = p.MasterBarang.NamaBrg, p.MasterBarang.JenisBarang, p.TanggalPO}).ToList
            Return Json(New With {.data = model}, JsonRequestBehavior.AllowGet)
        End Function

        '
        ' POST: /Purchase/Create
        <HttpPost()>
        Function Create(ByVal collection As FormCollection, ByVal model As TblBrgMasuk) As ActionResult
            Try
                If ModelState.IsValid Then
                    model.Save()
                End If
                Return RedirectToAction("Index")
            Catch ex As Exception
                Return View(model)
            End Try
        End Function
        
        '
        Function Delete(ByVal id As Integer) As JsonResult
            Dim stat As Integer = 0
            Try
                Dim data = (From s In ctx.TblBrgMasuks Where s.ID = id Select s).FirstOrDefault ' Selecting First or default data here
                ctx.DeleteObject(data)
                ctx.SaveChanges()
                stat = 1
            Catch ex As Exception
                Throw New HttpException(404, "NOT FOUND")
            End Try
            Return Json(New With {.stat = stat}, JsonRequestBehavior.AllowGet)
        End Function
    End Class
End Namespace