﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc

Namespace NiagaAgro_ERP
    Public Class MaterialController
        Inherits BaseController

        '
        ' GET: /Material
        <Authorize()>
        Function Index() As ActionResult
            ViewData("Unit") = (From s In ctx.TblSatuanUnits Select New With {s.ID, s.Satuan}).ToList
            Dim arrjenisBrg() As String = {"Pupuk", "Herbisida", "Insektisida", "BioAktifator Organik", "Alat Pertanian", _
                                           "Perlengkapan Pertanian", "Fungisida", "Racun", "Bibit"}
            Dim jenisbrg As List(Of String) = New List(Of String)(arrjenisBrg)
            ViewData("jenisbrg") = jenisbrg.ToList()
            Return View()
        End Function

        '
        ' GET: /Material/Details/5

        Function Details() As JsonResult
            Dim model = (From m In ctx.MasterBarangs Order By m.JenisBarang Ascending Select New With {m.ID, m.NamaBrg, m.Kemasan, m.IDUnit, m.TblSatuanUnit.Satuan, m.JenisBarang}).ToList
            Return Json(New With {.data = model}, JsonRequestBehavior.AllowGet)
        End Function

        '

        '
        ' POST: /Material/Create
        Function Create(ByVal collection As FormCollection, ByVal model As MasterBarang) As ActionResult
            Try
                If ModelState.IsValid Then
                    model.Save()
                End If
                Return RedirectToAction("Index")
            Catch ex As Exception
                Return View(model)
            End Try
        End Function

        '
        ' GET: /Material/Edit/5

        Function Delete(ByVal id As Integer) As JsonResult
            Dim stat As Integer = 0
            Try
                Dim data = (From s In ctx.MasterBarangs Where s.ID = id Select s).FirstOrDefault ' Selecting First or default data here
                ctx.DeleteObject(data)
                ctx.SaveChanges()
                stat = 1
            Catch ex As Exception
                Throw New HttpException(404, "NOT FOUND")
            End Try
            Return Json(New With {.stat = stat}, JsonRequestBehavior.AllowGet)
        End Function

        '
        ' POST: /Material/Delete/5


    End Class
End Namespace