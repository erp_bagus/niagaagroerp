﻿Imports System.Diagnostics.CodeAnalysis
Imports System.Security.Principal
Imports System.Web.Routing
Imports System.Net.Authorization
Imports NiagaAgro_ERP.BaseRepository
Namespace NiagaAgro_ERP
    Public Class UserManagementController
        Inherits System.Web.Mvc.Controller
        Dim ctx As New NiagaAgroEntities()
        Private userrepo As New UserManagementRepository
        '
        ' GET: /UserManagement

        Function GetById(ByVal username As String) As JsonResult
            userrepo = New UserManagementRepository
            Dim model = userrepo.GetUserById(username)
            Return Json(model)

        End Function
        Function GetUserInRoles(ByVal username As String) As JsonResult
            Dim model = Roles.GetRolesForUser(username)
            Return Json(model(0))
        End Function
        Function checkDuplicateName(ByVal username As String) As JsonResult
            Dim model = ctx.aspnet_Users.Where(Function(m) String.Equals(m.UserName, username)).FirstOrDefault()
            If model Is Nothing Then
                Return Json(True, JsonRequestBehavior.AllowGet)
            Else
                Return Json("Username already exists. Please fill another.", JsonRequestBehavior.AllowGet)
            End If
        End Function
        <Authorize()>
        Function Index() As ActionResult
            Dim users = Membership.GetAllUsers()

            Dim listmodel As New List(Of UserViewModel)
            For Each u As MembershipUser In users
                Dim newuser As New UserViewModel
                newuser.UserName = u.UserName
                newuser.Roles = Roles.GetRolesForUser(u.UserName)
                listmodel.Add(newuser)
            Next
            ViewData("listmodel") = listmodel
            Return View(New UserViewModel)
        End Function
        Public Function LogOn(returnUrl As String) As ActionResult
            ViewBag.ReturnUrl = returnUrl
            Return View()
        End Function

        '
        ' POST: /UserManagement/LogOn

        <HttpPost()> _
        Public Function LogOn(ByVal model As LogOnModel, ByVal returnUrl As String) As ActionResult
            If ModelState.IsValid Then
                If Membership.ValidateUser(model.UserName, model.Password) Then
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe)
                    If Url.IsLocalUrl(returnUrl) AndAlso returnUrl.Length > 1 AndAlso returnUrl.StartsWith("/") _
                       AndAlso Not returnUrl.StartsWith("//") AndAlso Not returnUrl.StartsWith("/\\") Then
                        Return Redirect(returnUrl)
                    Else
                        Return RedirectToAction("Index", "Home")
                    End If
                Else
                    ModelState.AddModelError("", "The user name or password provided is incorrect.")
                End If
            End If

            ' If we got this far, something failed, redisplay form
            Return View(model)
        End Function

        '
        ' GET: /UserManagement/LogOff

        Public Function LogOff() As ActionResult
            FormsAuthentication.SignOut()

            Return RedirectToAction("LogOn", "UserManagement")
        End Function

        '
        ' GET: /UserManagement/Register

        Public Function Register() As ActionResult
            Return View()
        End Function

        '
        ' POST: /UserManagement/Register

        <HttpPost()> _
        Public Function Register(ByVal model As UserViewModel) As ActionResult
            If Not (ModelState.IsValid) Then
                ' Attempt to register the user
                Dim createStatus As MembershipCreateStatus
                Membership.CreateUser(model.UserName, model.Password, model.Email, Nothing, Nothing, True, Nothing, createStatus)
                If model.Roles IsNot Nothing Then
                    Roles.AddUserToRoles(model.UserName, model.Roles)
                End If
                If createStatus = MembershipCreateStatus.Success Then
                    'FormsAuthentication.SetAuthCookie(model.UserName, False)
                    Return RedirectToAction("Index", "UserManagement")
                Else
                    ModelState.AddModelError("", ErrorCodeToString(createStatus))
                End If
            End If

            ' If we got this far, something failed, redisplay form
            Return View(model)
        End Function

        '
        ' GET: /UserManagement/ChangePassword

        <Authorize()> _
        Public Function ChangePassword() As ActionResult
            Return View()
        End Function

        '
        ' POST: /UserManagement/ChangePassword

        <HttpPost()> _
        Public Function ChangePassword(ByVal model As UserViewModel) As ActionResult
            Dim errObj = New With {.stat = 1, .err = New List(Of String)}
            Dim provider As MembershipProvider = Membership.Provider
            ' ChangePassword will throw an exception rather
            ' than return false in certain failure scenarios.
            Dim changePasswordSucceeded As Boolean

            Try
                If (model.UserName IsNot Nothing) Then
                    Dim currentUser As MembershipUser = Membership.GetUser(model.UserName, True)
                    Dim newPass As String = provider.ResetPassword(model.UserName, "")
                    changePasswordSucceeded = provider.ChangePassword(model.UserName, newPass, model.NewPassword)
                    'changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword)
                    For Each role In Roles.GetRolesForUser(model.UserName)
                        Roles.RemoveUserFromRole(model.UserName, role)
                    Next
                    Roles.AddUserToRoles(model.UserName, model.Roles)
                    errObj.stat = 1
                End If
            Catch ex As Exception
                changePasswordSucceeded = False
                errObj.stat = 0
            End Try

            If changePasswordSucceeded Then
                Return RedirectToAction("Index", "UserManagement")
            Else
                'ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.")
                errObj.err.Add("The current password is incorrect or the new password is invalid")
            End If


            ' If we got this far, something failed, redisplay form
            'Return View(model)
            Return Json(errObj, JsonRequestBehavior.AllowGet)
        End Function

        '
        ' GET: /UserManagement/ChangePasswordSuccess

        Public Function ChangePasswordSuccess() As ActionResult
            Return View()
        End Function

        <HttpPost()>
        Function Delete(ByVal username As String) As JsonResult
            Dim errObj = New With {.stat = 1, .err = New List(Of String)}
            Try
                If Not String.IsNullOrEmpty(username) Then
                    Membership.DeleteUser(username, True)
                    For Each role In Roles.GetRolesForUser(username)
                        Roles.RemoveUserFromRole(username, role)
                    Next
                    'userrepo.DeleteUser(model.UserId)
                Else
                    errObj.stat = 0
                    errObj.err.Add("Username tidak boleh kosong")
                End If
            Catch ex As Exception
                errObj.stat = 0
                errObj.err.Add(ex.Message)
            End Try
            Return Json(errObj, JsonRequestBehavior.AllowGet)
        End Function

#Region "Status Code"
        Public Function ErrorCodeToString(ByVal createStatus As MembershipCreateStatus) As String
            ' See http://go.microsoft.com/fwlink/?LinkID=177550 for
            ' a full list of status codes.
            Select Case createStatus
                Case MembershipCreateStatus.DuplicateUserName
                    Return "User name already exists. Please enter a different user name."

                Case MembershipCreateStatus.DuplicateEmail
                    Return "A user name for that e-mail address already exists. Please enter a different e-mail address."

                Case MembershipCreateStatus.InvalidPassword
                    Return "The password provided is invalid. Please enter a valid password value."

                Case MembershipCreateStatus.InvalidEmail
                    Return "The e-mail address provided is invalid. Please check the value and try again."

                Case MembershipCreateStatus.InvalidAnswer
                    Return "The password retrieval answer provided is invalid. Please check the value and try again."

                Case MembershipCreateStatus.InvalidQuestion
                    Return "The password retrieval question provided is invalid. Please check the value and try again."

                Case MembershipCreateStatus.InvalidUserName
                    Return "The user name provided is invalid. Please check the value and try again."

                Case MembershipCreateStatus.ProviderError
                    Return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator."

                Case MembershipCreateStatus.UserRejected
                    Return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator."

                Case Else
                    Return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator."
            End Select
        End Function
#End Region
    End Class
End Namespace
