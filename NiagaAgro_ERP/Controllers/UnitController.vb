﻿Namespace NiagaAgro_ERP
    Public Class UnitController
        Inherits BaseController

        '
        ' GET: /Unit
        <Authorize()>
        Function Index() As ActionResult
            Return View()
        End Function

        '
        ' GET: /Unit/Details/5

        Function Details() As JsonResult
            Dim model = (From s In ctx.TblSatuanUnits Select New With {s.ID, s.Satuan, s.Keterangan}).ToList()
            Return Json(New With {.data = model}, JsonRequestBehavior.AllowGet)
        End Function

        '
        ' GET: /Unit/Create


        '
        ' POST: /Unit/Create

        Function Create(ByVal collection As FormCollection, ByVal model As TblSatuanUnit) As ActionResult
            Try
                If ModelState.IsValid Then
                    model.Save()
                End If
                Return RedirectToAction("Index")
            Catch ex As Exception
                Return View(model)
            End Try
        End Function
        
        '
        ' GET: /Unit/Edit/5

      
        ' POST: /Unit/Delete/5

        Function Delete(ByVal id As Integer) As JsonResult
            Dim stat As Integer = 0
            Try
                Dim data = (From s In ctx.TblSatuanUnits Where s.ID = id Select s).FirstOrDefault ' Selecting First or default data here
                ctx.DeleteObject(data)
                ctx.SaveChanges()
                stat = 1
            Catch ex As Exception
                Throw New HttpException(404, "NOT FOUND")
            End Try
            Return Json(New With {.stat = stat}, JsonRequestBehavior.AllowGet)
        End Function
    End Class
End Namespace