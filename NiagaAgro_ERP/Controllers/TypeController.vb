﻿Namespace NiagaAgro_ERP
    Public Class TypeController
        Inherits BaseController

        '
        ' GET: /Type
        <Authorize()>
        Function Index() As ActionResult
            Return View()
        End Function

        Function Details() As JsonResult
            Dim model = (From t In ctx.TblTypes Select New With {t.ID, t.Type}).ToList
            Return Json(New With {.data = model}, JsonRequestBehavior.AllowGet)
        End Function

        Function Delete(ByVal id As Integer) As JsonResult
            Dim stat As Integer = 0
            Try
                Dim data = (From s In ctx.TblTypes Where s.ID = id Select s).FirstOrDefault ' Selecting First or default data here
                ctx.DeleteObject(data)
                ctx.SaveChanges()
                stat = 1
            Catch ex As Exception
                Throw New HttpException(404, "NOT FOUND")
            End Try
            Return Json(New With {.stat = stat}, JsonRequestBehavior.AllowGet)
        End Function

        Function Create(ByVal model As TblType) As JsonResult
            Dim stat As Integer = 0
            Dim message As String = ""
            If ModelState.IsValid Then
                Try
                    ' TODO: Add update logic here
                    model.Save()
                    stat = 1
                Catch ex As Exception
                    message = ex.Message
                End Try
            Else
                message = "Data Tidak valid!"
            End If
            Return Json(New With {.stat = stat, .data = message})
        End Function
    End Class
End Namespace