﻿Namespace NiagaAgro_ERP
    <Authorize()> _
     Public Class HomeController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Home

        Function Index() As ActionResult
            Return View()
        End Function

        Function getStock() As JsonResult
            Dim model As New List(Of StockBarang)
            Dim entities As New NiagaAgroEntities
            model = entities.ExecuteStoreQuery(Of StockBarang)(
                  "  DECLARE @TempTable TABLE( " &
                  "  IDBarang	 Integer, " &
                  "  JenisBarang Varchar(255)," &
                  "  Jumlah		Integer," &
                  "  NamaBrg	 Varchar(255)," &
                  "  TotalStock	 Integer) " &
                  "  INSERT INTO @TempTable " &
                  "  SELECT	mb.ID, " &
                  "         mb.JenisBarang," &
                  "        SUM(ISNULL(CAST(pd.Jumlah AS INTEGER),0)) [Jumlah], " &
                  "          mb.NamaBrg, " &
                  "          bm.Jumlah [TotalStock]" &
                  "  FROM [NiagaAgro].[dbo].TblPenjualanDetail pd " &
                  "     FULL JOIN dbo.[TblBrgMasuk] bm on bm.IDBarang = pd.IDBarang " &
                  "     INNER JOIN dbo.MasterBarang mb on bm.IDBarang = mb.ID " &
                  "  GROUP BY mb.ID,bm.IDBarang,mb.NamaBrg,bm.Jumlah,mb.JenisBarang " &
                  "  select IDBarang,JenisBarang,NamaBrg,TotalStock [JlhMasuk], Jumlah [JlhKeluar], (TotalStock - Jumlah) [stock] From @TempTable "
            ).ToList
            Return Json(New With {.data = model}, JsonRequestBehavior.AllowGet)
        End Function
    End Class
End Namespace