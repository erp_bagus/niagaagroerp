﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc
Imports NiagaAgro_ERP.ModelCustomer

Namespace NiagaAgro_ERP
    Public Class CustomerController
        Inherits BaseController

        '
        ' GET: /Customer

        Function Details() As JsonResult
            Dim model = (From c In ctx.TblCustomers Select New With {.ID = c.ID, .Nama = c.Nama, .Alamat = c.Alamat, .Phone = c.Phone, .JenisKelamin = c.JenisKelamin}).ToList()
            Return Json(New With {.data = model}, JsonRequestBehavior.AllowGet)
        End Function
        <Authorize()>
        Function Index() As ActionResult

            Dim jk As New Dictionary(Of String, String)
            jk.Add("Laki-Laki", "Laki-Laki")
            jk.Add("Wanita", "Wanita")
            ViewData("JenisKelamin") = New SelectList(jk.ToList(), "Key", "Value")

            Return View()
        End Function
        <HttpPost()>
        Function Save(ByVal model As TblCustomer) As ActionResult
            Try
                If ModelState.IsValid Then
                    model.Save()
                End If
                Return RedirectToAction("Index")
            Catch ex As Exception
                Return View(model)
            End Try
        End Function

        Function Delete(ByVal id As Integer) As ActionResult
            Dim stat As Integer = 0
            Try
                Dim data = (From s In ctx.TblCustomers Where s.ID = id Select s).FirstOrDefault ' Selecting First or default data here
                ctx.DeleteObject(data)
                ctx.SaveChanges()
                stat = 1
            Catch ex As Exception
                Throw New HttpException(404, "NOT FOUND")
            End Try
            Return Json(New With {.stat = stat}, JsonRequestBehavior.AllowGet)
        End Function

    End Class
End Namespace
