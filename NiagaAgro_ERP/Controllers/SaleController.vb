﻿Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports Microsoft.ReportingServices
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Namespace NiagaAgro_ERP
    Public Class SaleController
        Inherits BaseController

        '
        ' GET: /Sale
        <Authorize()>
        Function Index() As ActionResult
            ViewData("Barang") = (From b In ctx.MasterBarangs Select New With {b.ID, b.NamaBrg}).ToList
            ViewData("IDCustomer") = (From c In ctx.TblCustomers Select New With {c.ID, c.Nama}).ToList
            ViewData("Type") = (From t In ctx.TblTypes Select New With {t.ID, t.Type}).ToList
            ViewData("IDBarang") = (From b In ctx.TblBrgMasuks Select New With {b.IDBarang, b.MasterBarang.NamaBrg}).ToList()
            Dim model = (From p In ctx.TblPenjualans Select p).ToList

            'FORM NUMBER
            Dim CodeNumber As String
            Dim Code = (From a In ctx.TblPenjualans
                        Select a.FormNumber.Substring(5, 6)).Max()
            Dim No As Integer
            Dim Number As String
            If Code Is Nothing Then
                No = 1
                Number = String.Format("{0:000000}", CDbl(No))
            Else
                No = CInt(Code)
                Number = String.Format("{0:000000}", (CDbl(No) + 1))
            End If
            CodeNumber = ("FORM-" + Number.ToString())
            ViewData("FormNumber") = CodeNumber
            ' end form number
            Return View(model)
        End Function

        '
        ' GET: /Sale/Details/5

        Function Details() As JsonResult
            Dim model = (From p In ctx.TblPenjualans Join t In ctx.TblTypes On t.ID Equals (p.Type) Select New With {p.FormNumber, p.TblCustomer.Nama, p.Tanggal, t.Type})
            Return Json(New With {.data = model}, JsonRequestBehavior.AllowGet)
        End Function
        Function GetNamaBarang() As JsonResult
            Dim model = (From b In ctx.TblBrgMasuks Select New With {.Id = b.IDBarang, .Name = b.MasterBarang.NamaBrg}).ToList()
            Return Json(model, JsonRequestBehavior.AllowGet)
        End Function
        Function GetIdBarang(ByVal id As Integer) As JsonResult
            Dim model = (From b In ctx.TblBrgMasuks Where b.IDBarang = id Select New With {.Id = b.IDBarang, .Name = b.MasterBarang.NamaBrg}).ToList()
            Return Json(model, JsonRequestBehavior.AllowGet)
        End Function
        Function GetPenjualanData(ByVal id As Integer) As JsonResult
            Dim model = (From p In ctx.TblPenjualans Where p.ID = id
                         Select New With {.ID = p.ID,
                                          .FormNumber = p.FormNumber,
                                          .IDCustomer = p.IDCustomer,
                                          .Tanggal = p.Tanggal,
                                          .Type = p.Type}).FirstOrDefault()
            Return Json(model, JsonRequestBehavior.AllowGet)
        End Function
        Function GetPenjualanDetailData(ByVal id As Integer) As JsonResult
            Dim model = (From pd In ctx.TblPenjualanDetails Where pd.IDPenjualan = id
                         Select New With {.ID = pd.ID,
                                          .IDBarang = pd.IDBarang,
                                          .Jumlah = pd.Jumlah,
                                          .HargaJual = pd.HargaJual,
                                          .Tanggal = pd.Tanggal,
                                          .Keterangan = pd.Keterangan}).ToList()
            Return Json(model, JsonRequestBehavior.AllowGet)
        End Function
        Function GetIDCustomer(ByVal id As Integer) As JsonResult
            Dim model = (From c In ctx.TblCustomers Where c.ID = id Select New ModelCustomer With {.ID = c.ID, .Nama = c.Nama}).FirstOrDefault()
            Return Json(model, JsonRequestBehavior.AllowGet)
        End Function
        Function GetIDType(ByVal id As Integer) As JsonResult
            Dim model = (From c In ctx.TblTypes Where c.ID = id Select New With {.ID = c.ID, .Type = c.Type}).FirstOrDefault()
            Return Json(model, JsonRequestBehavior.AllowGet)
        End Function
        <Authorize()>
        Function Create() As ActionResult
            ViewData("Barang") = (From b In ctx.MasterBarangs Select New With {b.ID, b.NamaBrg}).ToList
            ViewData("IDCustomer") = (From c In ctx.TblCustomers Select New With {c.ID, c.Nama}).ToList
            ViewData("Type") = (From t In ctx.TblTypes Select New With {t.ID, t.Type}).ToList
            ViewData("IDBarang") = (From b In ctx.TblBrgMasuks Select New With {b.IDBarang, b.MasterBarang.NamaBrg}).ToList()
            'Dim model = (From p In ctx.TblPenjualans Select p).ToList

            'FORM NUMBER
            Dim CodeNumber As String
            Dim Code = (From a In ctx.TblPenjualans
                        Select a.FormNumber.Substring(5, 6)).Max()
            Dim No As Integer
            Dim Number As String
            If Code Is Nothing Then
                No = 1
                Number = String.Format("{0:000000}", CDbl(No))
            Else
                No = CInt(Code)
                Number = String.Format("{0:000000}", (CDbl(No) + 1))
            End If
            CodeNumber = ("FORM-" + Number.ToString())
            ViewData("FormNumber") = CodeNumber
            Return View()
        End Function
        Function Save(ByVal model As TblPenjualan, ByVal listpenjualan As List(Of TblPenjualanDetail)) As ActionResult
            Dim stat As Integer = 0
            Dim intState As Integer = 0
            Dim errObj = New With {.stat = 1, .message = New List(Of Object)}
            Dim errorMessage As String = "UNKNOWN ERROR"

            With errObj
                If listpenjualan IsNot Nothing Then
                    If model.Type = 5 Then
                        If model.IDCustomer Is Nothing Then
                            .stat = 0
                            .message.Add(New With {.Key = "Type", .Value = "Jika Transaksi Bon, Nama harus diisi!"})
                        End If
                        If model.DueDate Is Nothing Then
                            .stat = 0
                            .message.Add(New With {.Key = "Type", .Value = "Tanggal jatuh tempo harus diisi!"})
                        End If
                    End If
                    If Not (IsNothing(Request.Form("TempTotalBelanja"))) Then
                        If model.Type = 4 Then
                            If model.Buy < (Request.Form("TempTotalBelanja")) Then
                                .stat = 0
                                .message.Add(New With {.Key = "Type", .Value = "Jika transaksi Bayar Cash tidak boleh lebih kecil dari Total Belanja!"})
                            End If
                        End If
                    End If
                    If model.Type = 4 Then
                        If model.Buy Is Nothing Then
                            .stat = 0
                            .message.Add(New With {.Key = "Type", .Value = "Bayar Tidak boleh kosong!"})
                        End If
                    End If
                    For Each msg In listpenjualan
                        Dim cnt = listpenjualan.IndexOf(msg)
                        If (msg.IDBarang = Nothing) Then msg.IDBarang = 0
                        If msg.IDBarang = 0 Then
                            .stat = 0
                            .message.Add(New With {.Key = "Type", .Value = "Nama Barang baris ke-" & cnt + 1 & " Tidak boleh kosong!"})
                        End If
                        If (msg.Jumlah = Nothing) Then msg.Jumlah = 0
                        If msg.Jumlah = 0 Then
                            .stat = 0
                            .message.Add(New With {.Key = "Type", .Value = "Jumlah baris ke-" & cnt + 1 & " Tidak boleh kosong!"})
                        End If
                        If (msg.HargaJual = Nothing) Then msg.HargaJual = 0
                        If msg.HargaJual = 0 Then
                            .stat = 0
                            .message.Add(New With {.Key = "Type", .Value = "Harga Jual baris ke-" & cnt + 1 & " Tidak boleh kosong!"})
                        End If

                    Next
                End If
            End With

            If errObj.message.Count > 0 Then
                Return Json(errObj)
            End If
            model.UangMuka = model.Buy
            ctx.TblPenjualans.AddObject(model)

            If model.ID <> 0 Then
                ctx.ObjectStateManager.ChangeObjectState(model, EntityState.Modified)
                If listpenjualan IsNot Nothing Then
                    ctx.ExecuteStoreCommand("DELETE FROM TblPenjualanDetail WHERE IDPenjualan = @id", New SqlParameter("@id", model.ID))
                End If
            End If

            Try
                If ctx.SaveChanges() > 0 Then
                    If listpenjualan IsNot Nothing Then
                        For Each e In listpenjualan
                            If e.IDBarang <> 0 Then
                                Dim r As New TblPenjualanDetail
                                r.IDPenjualan = model.ID
                                r.IDBarang = e.IDBarang
                                r.Tanggal = model.Tanggal
                                r.Jumlah = e.Jumlah
                                r.HargaJual = e.HargaJual
                                r.Keterangan = e.Keterangan
                                ctx.TblPenjualanDetails.AddObject(r)
                                ctx.SaveChanges()
                            End If
                        Next
                    End If

                    Return Json(New With {.stat = 1, .message = {model.ID}})
                End If
            Catch ex As Exception
                Return Json(New With {.stat = 0, .message = {ex.Message, ex.InnerException.Message}})
            Finally
                ctx.Dispose()
            End Try

            Return Json(New With {.stat = 0, .message = {errorMessage}})
        End Function
        <Authorize()>
        Function Edit(ByVal id As Integer) As ActionResult
            Dim model = (From p In ctx.TblPenjualans Where p.ID = id
                       Select p).FirstOrDefault()
            Dim idcustomer = (From c In ctx.TblCustomers Select New With {.ID = c.ID, .Nama = c.Nama}).ToList()
            Dim type = (From t In ctx.TblTypes Select New With {t.ID, t.Type}).ToList
            Dim dp = (From pd In ctx.TblPenjualanDetails Where pd.IDPenjualan = id
                        Select pd).ToList()
            ViewData("listsale") = dp
            ViewData("IDCustomer") = New SelectList(idcustomer, "ID", "Nama", IIf(IsNothing(model.IDCustomer), "", model.IDCustomer))
            ViewData("Type") = New SelectList(type, "ID", "Type", model.Type)
            ViewData("IDBarang") = (From b In ctx.TblBrgMasuks Select New With {b.IDBarang, b.MasterBarang.NamaBrg}).ToList()
            Return View(model)
        End Function
        '
        ' GET: /Sale/Edit/5

        Function DeletePenjualan(ByVal id As Integer) As ActionResult
            Dim rvalue = New With {.stat = 1, .message = "Data terhapus"}

            Try
                ctx.ExecuteStoreCommand("DELETE FROM TblPenjualanDetail WHERE IDPenjualan=@id", New SqlParameter("@id", id))
                ctx.ExecuteStoreCommand("DELETE FROM TblPenjualan WHERE ID=@id", New SqlParameter("@id", id))
            Catch ex As Exception
                rvalue.stat = 0
                rvalue.message = "Error: " & ex.Message
            Finally
                ctx.Dispose()
            End Try

            Return Json(rvalue)
        End Function
        <Authorize()>
        Function PrintSaleRecap() As ActionResult
            Dim buytype = (From t In ctx.TblTypes Select New With {t.ID, t.Type}).ToList
            ViewData("Type") = New SelectList(buytype, "Type", "Type")
            Return View()
        End Function
        <HttpPost()>
        Function PrintSaleRecap(ByVal tglawal As Date?, ByVal tglakhir As Date?, ByVal type As String) As ActionResult
            Dim mimeType As String = "application/pdf"
            Dim Encoding As String = Nothing
            Dim fileNameExtension As String = Nothing
            Dim streams As String() = Nothing
            Dim warnings() As Microsoft.Reporting.WebForms.Warning = Nothing
            Dim firstTime = tglawal.Value.ToString("dd/MM/yyyy", CultureInfo.CreateSpecificCulture("id-ID"))
            Dim secondTime = tglakhir.Value.ToString("dd/MM/yyyy", CultureInfo.CreateSpecificCulture("id-ID"))
            Dim r As New Microsoft.Reporting.WebForms.LocalReport
            r.EnableExternalImages = True
            r.ReportPath = Server.MapPath("~/Content/Report/SaleRecapitulation.rdlc")

            r.SetParameters(New ReportParameter("TanggalAwal", firstTime))
            r.SetParameters(New ReportParameter("TanggalAkhir", secondTime))
            r.SetParameters(New ReportParameter("Type", type))
            r.DataSources.Add(New ReportDataSource("DataSet1", ctx.RekapitulasiPenjualan(firstTime, secondTime, type)))

            r.Refresh()
            Dim reportType As String = "PDF"
            Dim deviceInfo As String = "<DeviceInfo>" &
            "  <OutputFormat>PDF</OutputFormat>" &
            "  <PageWidth>8.27in</PageWidth>" &
            "  <PageHeight>11.69in</PageHeight>" &
            "  <MarginTop>0.00in</MarginTop>" &
            "  <MarginLeft>0.00in</MarginLeft>" &
            "  <MarginRight>0.00in</MarginRight>" &
            "  <MarginBottom>0.00in</MarginBottom>" &
            "</DeviceInfo>"

            Dim output() As Byte = r.Render(reportType, deviceInfo, mimeType, Encoding, fileNameExtension, streams, warnings)

            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=Penjualan_" & tglawal.Value.ToString("dd/MM/yyyy") & "s.d" & tglakhir.Value.ToString("dd/MM/yyyy") & "_.pdf"))
            Response.BinaryWrite(output.ToArray())
            Return Nothing

        End Function
        <Authorize()>
        Function Paid(ByVal id As Integer) As ActionResult
            Dim model = (From p In ctx.TblPenjualans Where p.ID = id
                          Select p).FirstOrDefault()
            Dim data = (From p In ctx.TblPenjualans Where p.ID = id
                          Select p).ToList()
            Dim idcustomer = (From c In ctx.TblCustomers Select New With {.ID = c.ID, .Nama = c.Nama}).ToList()
            Dim type = (From t In ctx.TblTypes Select New With {t.ID, t.Type}).ToList
            Dim dp = (From pd In ctx.TblPenjualanDetails Where pd.IDPenjualan = id
                        Select pd).ToList()
            Dim paiddata = (From a In ctx.TblPelunasans Where a.IDPenjualan = id
                            Select a).ToList()
            Dim cekbayar = (From a In ctx.TblPelunasans Where a.IDPenjualan = id
                            Select a).FirstOrDefault()

            If (Not IsNothing(cekbayar)) Then
                Dim maxbayar = Aggregate m In ctx.TblPelunasans
                                          Where m.IDPenjualan = id
                                          Into Sum(m.Bayar)
                ViewData("maxbayar") = maxbayar
            End If
            ViewData("paidlist") = paiddata
            ViewData("data") = data
            ViewData("listsale") = dp
            ViewData("IDCustomer") = New SelectList(idcustomer, "ID", "Nama", IIf(IsNothing(model.IDCustomer), "", model.IDCustomer))
            ViewData("Type") = New SelectList(type, "ID", "Type", model.Type)
            ViewData("IDBarang") = (From b In ctx.TblBrgMasuks Select New With {b.IDBarang, b.MasterBarang.NamaBrg}).ToList()
            Return View()
        End Function
        <HttpPost()>
        Function SavePaid(ByVal model As TblPelunasan) As ActionResult
            Try
                If ModelState.IsValid Then
                    Dim sale = (From s In ctx.TblPenjualans Where s.ID = model.IDPenjualan
                                Select s).FirstOrDefault()
                    Dim pd = ctx.TblPenjualanDetails.Where(Function(s) s.IDPenjualan = model.IDPenjualan).
                        Sum(Function(s) s.HargaJual)
                    Dim jumlahbarang = ctx.TblPenjualanDetails.Where(Function(s) s.IDPenjualan = model.IDPenjualan).
                       Sum(Function(s) s.Jumlah)

                    Dim totalsale = Request.Form("jumlahhargabelanja")
                    ctx.TblPelunasans.AddObject(model)
                    If model.ID > 0 Then
                        ctx.ObjectStateManager.ChangeObjectState(model.ID, Data.EntityState.Modified)
                    End If
                    ctx.SaveChanges()

                    Dim paid = Aggregate m In ctx.TblPelunasans
                               Where m.IDPenjualan = model.IDPenjualan
                               Into Sum(m.Bayar)
                    If (Not IsNothing(sale.Buy)) Then
                        Dim total = (sale.Buy + paid)
                        If (total >= totalsale) Then
                            Dim sqlUpdate As String = "UPDATE TblPenjualan SET Type = 4," &
                                        "Buy = @Buy, DueDate = NULL WHERE ID=@id;"
                            ctx.ExecuteStoreCommand(sqlUpdate, New SqlParameter("@id", model.IDPenjualan),
                                                   New SqlParameter("@Buy", pd))
                        End If
                    Else
                        If (paid >= totalsale) Then
                            Dim sqlUpdate As String = "UPDATE TblPenjualan SET Type = 4," &
                                        "Buy = @, DueDate = NULL WHERE ID=@id;"
                            ctx.ExecuteStoreCommand(sqlUpdate, New SqlParameter("@id", model.IDPenjualan),
                                                    New SqlParameter("@Buy", pd))
                        End If
                    End If
                End If
                Return RedirectToAction("Index")
            Catch ex As Exception
                Return View(model)
            End Try
            Return View()
        End Function
        <Authorize()>
        Function PrintSalePaid() As ActionResult
            Dim buytype = (From t In ctx.TblTypes Select New With {t.ID, t.Type}).ToList
            ViewData("Type") = New SelectList(buytype, "Type", "Type")
            ViewData("FormNumber") = New SelectList((From f In ctx.TblPenjualans Select New With {f.FormNumber}).ToList, "FormNumber", "FormNumber")
            Return View()
        End Function
        <HttpPost()>
        Function PrintSalePerPaid(ByVal tgl As Date, ByVal type As String, ByVal formnumber As String) As ActionResult
            Dim mimeType As String = "application/pdf"
            Dim Encoding As String = Nothing
            Dim fileNameExtension As String = Nothing
            Dim streams As String() = Nothing
            Dim warnings() As Microsoft.Reporting.WebForms.Warning = Nothing
            Dim time_local = tgl.ToString("dd/MM/yyyy", CultureInfo.CreateSpecificCulture("id-ID"))
            Dim r As New Microsoft.Reporting.WebForms.LocalReport
            r.EnableExternalImages = True
            r.ReportPath = Server.MapPath("~/Content/Report/RecapUtangPelanggan.rdlc")

            r.SetParameters(New ReportParameter("Tanggal", time_local))
            r.SetParameters(New ReportParameter("Type", type))
            r.SetParameters(New ReportParameter("FormNumber", formnumber))
            r.DataSources.Add(New ReportDataSource("DsUtang", ctx.RekapitulasiUtang(time_local, type, formnumber)))

            r.Refresh()
            Dim reportType As String = "PDF"
            Dim deviceInfo As String = "<DeviceInfo>" &
            "  <OutputFormat>PDF</OutputFormat>" &
            "  <PageWidth>8.27in</PageWidth>" &
            "  <PageHeight>11.69in</PageHeight>" &
            "  <MarginTop>0.00in</MarginTop>" &
            "  <MarginLeft>0.00in</MarginLeft>" &
            "  <MarginRight>0.00in</MarginRight>" &
            "  <MarginBottom>0.00in</MarginBottom>" &
            "</DeviceInfo>"

            Dim output() As Byte = r.Render(reportType, deviceInfo, mimeType, Encoding, fileNameExtension, streams, warnings)

            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=Pelunasan" & CDate(time_local).ToString("dd-MM-yyyy") & ".pdf"))
            Response.BinaryWrite(output.ToArray())
            Return Nothing

        End Function
        <Authorize()>
        Function PrintAllDebt() As ActionResult
            Dim buytype = (From t In ctx.TblTypes Select New With {t.ID, t.Type}).ToList
            ViewData("Type") = New SelectList(buytype, "Type", "Type")
            Return View()
        End Function
        <HttpPost()>
        Function PrintAllDebt(ByVal tglawal As Date?, ByVal tglakhir As Date?, ByVal type As String) As ActionResult
            Dim mimeType As String = "application/pdf"
            Dim Encoding As String = Nothing
            Dim fileNameExtension As String = Nothing
            Dim streams As String() = Nothing
            Dim warnings() As Microsoft.Reporting.WebForms.Warning = Nothing
            Dim firstTime = tglawal.Value.ToString("dd/MM/yyyy", CultureInfo.CreateSpecificCulture("id-ID"))
            Dim secondTime = tglakhir.Value.ToString("dd/MM/yyyy", CultureInfo.CreateSpecificCulture("id-ID"))
            Dim r As New Microsoft.Reporting.WebForms.LocalReport
            r.EnableExternalImages = True
            r.ReportPath = Server.MapPath("~/Content/Report/RecapUtang.rdlc")

            r.SetParameters(New ReportParameter("TanggalAwal", firstTime))
            r.SetParameters(New ReportParameter("TanggalAkhir", secondTime))
            r.SetParameters(New ReportParameter("Type", type))
            r.DataSources.Add(New ReportDataSource("DsUtang", ctx.ProdRekapitulasiUtang(firstTime, secondTime, type)))

            r.Refresh()
            Dim reportType As String = "PDF"
            Dim deviceInfo As String = "<DeviceInfo>" &
            "  <OutputFormat>PDF</OutputFormat>" &
            "  <PageWidth>8.27in</PageWidth>" &
            "  <PageHeight>11.69in</PageHeight>" &
            "  <MarginTop>0.00in</MarginTop>" &
            "  <MarginLeft>0.00in</MarginLeft>" &
            "  <MarginRight>0.00in</MarginRight>" &
            "  <MarginBottom>0.00in</MarginBottom>" &
            "</DeviceInfo>"

            Dim output() As Byte = r.Render(reportType, deviceInfo, mimeType, Encoding, fileNameExtension, streams, warnings)

            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=Pelunasan_" & tglawal.Value.ToString("dd/MM/yyyy") & "-" & tglakhir.Value.ToString("dd/MM/yyyy") & "_.pdf"))
            Response.BinaryWrite(output.ToArray())
            Return Nothing

        End Function
        Public Function PrintKwitansi(ByVal id As Integer) As ActionResult
            Dim mimeType As String = "application/pdf"
            Dim Encoding As String = Nothing
            Dim fileNameExtension As String = Nothing
            Dim streams As String() = Nothing
            Dim warnings() As Microsoft.Reporting.WebForms.Warning = Nothing
            Dim r As New Microsoft.Reporting.WebForms.LocalReport
            Dim totalhutang As Decimal
            Dim sisahutang As Decimal
            Dim bayaruang As Decimal
            Dim tgl As String
            Dim jthtempo As String
            Dim bayar = (From p In ctx.TblPenjualans Where p.ID = id Select p.Buy).FirstOrDefault
            Dim jatuhtempo = (From p In ctx.TblPenjualans Where p.ID = id Select p.DueDate).FirstOrDefault
            Dim totaljual = ctx.ProcKwitansiListBarang(id).Select(Function(m) m.TotalJual).Sum()
            Dim bayarcicil = ctx.TblPelunasans.Where(Function(m) m.IDPenjualan = id).Select(Function(s) s.Bayar).FirstOrDefault
            Dim tglangsuran = (From d In ctx.TblPelunasans Where d.IDPenjualan = id
                               Select New With {.Tanggal = d.Tanggal}).FirstOrDefault
            Dim totalangsuran As Nullable(Of Decimal) = (Aggregate t In ctx.TblPelunasans
                      Where t.IDPenjualan = id
                      Into Sum(CType(t.Bayar, Decimal?)))
            Dim sisaangsuran As Nullable(Of Decimal) = (Aggregate t In ctx.TblPelunasans
                      Where t.IDPenjualan = id
                      Into Sum(CType(t.Sisa, Decimal?)))
            If Not (IsNothing(bayar)) Then
                totalhutang = totaljual - bayar
            End If
            If Not (IsNothing(totalangsuran)) Then
                sisahutang = totaljual - bayar - totalangsuran
            End If
            If (bayarcicil <> 0) Then
                bayaruang = bayarcicil
            Else
                bayaruang = bayar
            End If
            If Not (IsNothing(tglangsuran)) Then
                tgl = (From d In ctx.TblPelunasans Where d.IDPenjualan = id Select d.Tanggal).Max.ToString("dd/MM/yyyy")
            Else
                tgl = "-"
            End If
            If Not (IsNothing(totalangsuran)) Then
                totalangsuran = totalangsuran
            Else
                totalangsuran = 0
            End If
            If Not (IsNothing(jatuhtempo)) Then
                jthtempo = jatuhtempo
            Else
                jthtempo = "-"
            End If
            r.EnableExternalImages = True
            r.ReportPath = Server.MapPath("~/Content/Report/KwitansiPenjualan.rdlc")
            r.SetParameters(New ReportParameter("totalhutang", totalhutang))
            r.SetParameters(New ReportParameter("totalangsuran", totalangsuran))
            r.SetParameters(New ReportParameter("sisahutang", sisahutang))
            r.SetParameters(New ReportParameter("bayarterbilang", TERBILANG(bayaruang)))
            r.SetParameters(New ReportParameter("tglangsuran", tgl))
            r.SetParameters(New ReportParameter("jatuhtempo", jthtempo))
            r.DataSources.Add(New ReportDataSource("DsPrintKwitansi", ctx.ProcPrintKwitansi(id)))
            r.DataSources.Add(New ReportDataSource("DsListBarang", ctx.ProcKwitansiListBarang(id)))
            r.Refresh()
            Dim reportType As String = "PDF"
            Dim deviceInfo As String = "<DeviceInfo>" +
                "<ColorDepth>32</ColorDepth><DpiX>350</DpiX><DpiY>350</DpiY><OutputFormat>PDF</OutputFormat>" +
                "  <PageWidth>8.27in</PageWidth>" +
                "  <PageHeight>11.69in</PageHeight>" +
                "  <MarginTop>0.5in</MarginTop>" +
                "  <MarginLeft>0.5in</MarginLeft>" +
                "  <MarginRight>0in</MarginRight>" +
                "  <MarginBottom>0in</MarginBottom>" +
                "</DeviceInfo>"

            Dim output() As Byte = r.Render(reportType, deviceInfo, mimeType, Encoding, fileNameExtension, streams, warnings)

            'Response.ContentType = "application/pdf"
            'Response.AddHeader("Content-Disposition", String.Format("attachment;filename=Kwitansi_" & pay.TblCustomer.Nama & "_" & pay.FormNumber & "_.pdf"))
            'Response.BinaryWrite(output.ToArray())
            'Return Nothing
            Return File(output, mimeType)
            Return View()
        End Function
        Public Function TERBILANG(ByVal x As Decimal) As String
            Dim tampung As Double
            Dim teks As String
            Dim bagian As String
            Dim i As Integer
            Dim tanda As Boolean

            Dim letak(5)
            letak(1) = "RIBU "
            letak(2) = "JUTA "
            letak(3) = "MILYAR "
            letak(4) = "TRILYUN "

            If (x < 0) Then
                TERBILANG = ""
                Exit Function
            End If

            If (x = 0) Then
                TERBILANG = "NOL"
                Exit Function
            End If

            If (x < 2000) Then
                tanda = True
            End If
            teks = ""

            If (x >= 1.0E+15) Then
                TERBILANG = "NILAI TERLALU BESAR"
                Exit Function
            End If

            For i = 4 To 1 Step -1
                tampung = Int(x / (10 ^ (3 * i)))
                If (tampung > 0) Then
                    bagian = ratusan(tampung, tanda)
                    teks = teks & bagian & letak(i)
                End If
                x = x - tampung * (10 ^ (3 * i))
            Next

            teks = teks & ratusan(x, False)
            TERBILANG = teks & "RUPIAH"
        End Function

        Function ratusan(ByVal y As Double, ByVal flag As Boolean) As String
            Dim tmp As Double
            Dim bilang As String
            Dim bag As String
            Dim j As Integer

            Dim angka(9)
            angka(1) = "SE"
            angka(2) = "DUA "
            angka(3) = "TIGA "
            angka(4) = "EMPAT "
            angka(5) = "LIMA "
            angka(6) = "ENAM "
            angka(7) = "TUJUH "
            angka(8) = "DELAPAN "
            angka(9) = "SEMBILAN "

            Dim posisi(2)
            posisi(1) = "PULUH "
            posisi(2) = "RATUS "

            bilang = ""
            For j = 2 To 1 Step -1
                tmp = Int(y / (10 ^ j))
                If (tmp > 0) Then
                    bag = angka(tmp)
                    If (j = 1 And tmp = 1) Then
                        y = y - tmp * 10 ^ j
                        If (y >= 1) Then
                            posisi(j) = "BELAS "
                        Else
                            angka(y) = "SE"
                        End If
                        bilang = bilang & angka(y) & posisi(j)
                        ratusan = bilang
                        Exit Function
                    Else
                        bilang = bilang & bag & posisi(j)
                    End If
                End If
                y = y - tmp * 10 ^ j
            Next

            If (flag = False) Then
                angka(1) = "SATU "
            End If
            bilang = bilang & angka(y)
            ratusan = bilang
        End Function
    End Class
End Namespace