﻿Imports System.Globalization

Namespace Common
    Public Module EditDialogTriggerHelper
        <System.Runtime.CompilerServices.Extension()> _
        Public Function EditDialogTrigger(ByVal helper As HtmlHelper, ByVal id As Integer, ByVal action As String) As System.Web.HtmlString
            If helper.ViewContext.HttpContext.User.Identity.IsAuthenticated Then
                Return helper.ActionLink(" ", action, New With {.id = id}, New With {.class = "icon-edit", .Title = "Ubah"})
            End If
            Return New HtmlString("")
        End Function
        <System.Runtime.CompilerServices.Extension()> _
        Public Function DeleteDialogTrigger(ByVal helper As HtmlHelper, ByVal id As Integer, ByVal action As String) As HtmlString
            If helper.ViewContext.HttpContext.User.Identity.IsAuthenticated Then
                Return New HtmlString("<a href='#' class='icon-remove' onclick='return " &
                                 "removedata(" & id.ToString() & ",""" & action & """);'></a>")
            End If
            Return New HtmlString("")
        End Function

        <System.Runtime.CompilerServices.Extension()> _
        Public Sub WriteAccordionBegin(ByVal helper As HtmlHelper, ByVal Id As String, ByVal title As String)
            Dim Writer = helper.ViewContext.Writer
            Writer.Write("<div id='" & Id & "' class='accordion'>" & vbCrLf)
            Writer.Write("     <div class='accordion-group box'>" & vbCrLf)
            Writer.Write("         <div class='accordion-heading title'>" & vbCrLf)
            Writer.Write("             <h2>" & vbCrLf)
            Writer.Write("                 <span>" & title & "</span>" & vbCrLf)
            Writer.Write("             </h2>" & vbCrLf)
            Writer.Write("             <a href='#" & Id & "body' data-parent='#" & Id & "' " & vbCrLf)
            Writer.Write("	data-toggle='collapse' class='accordion-toggle minimize pull-right'>" & vbCrLf)
            Writer.Write("             </a>" & vbCrLf)
            Writer.Write("         </div>" & vbCrLf)
            Writer.Write("         <div class='accordion-body collapse' style='height: 0px;' id='" & Id & "body'>" & vbCrLf)
            Writer.Write("             <div class='content noPad'>" & vbCrLf)
        End Sub
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub WriteAccordionEnd(ByVal helper As HtmlHelper, ByVal nipp As String, ByVal action As String)

            Dim writer = helper.ViewContext.Writer
            writer.Write("</div>" & vbCrLf)

            writer.Write(helper.ActionLink("Tambah", action, New With {.nipp = nipp},
                                           New With {.class = "btn btn-info dropdown-toggle pull-right", .style = "margin: 5px 10px;"}))

            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)

        End Sub
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub WriteAccordionEnd(ByVal helper As HtmlHelper)
            Dim writer = helper.ViewContext.Writer
            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)
        End Sub
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub WriteAccordionEnd1(ByVal helper As HtmlHelper, ByVal frmName As String)

            Dim writer = helper.ViewContext.Writer
            writer.Write("</div>" & vbCrLf)
            writer.Write("<a class=""btn btn-info dropdown-toggle pull-right"" " & vbCrLf)
            writer.Write("style=""margin: 5px 10px;"" onclick=""return " & frmName & ".showdialog(null);"">" & vbCrLf)
            writer.Write("    Tambah</a>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)
        End Sub


        <System.Runtime.CompilerServices.Extension()> _
        Public Function WriteDataDialogBinder(ByVal helper As HtmlHelper, ByVal frmName As String, ByVal obj As Type) As System.Web.HtmlString
            Dim properties() As System.Reflection.PropertyInfo = obj.GetProperties()
            Dim ls As New List(Of String)
            ls.Add("<script type='text/javascript'>")
            ls.Add("bind_" & frmName & " = function(element)")
            ls.Add("{")
            For Each prop As System.Reflection.PropertyInfo In properties
                If prop.PropertyType.Namespace.Equals("System") Then
                    ls.Add(String.Format("setElementValue( $('#{0}_{1}') , $(element).attr('data-{1}') );", frmName, prop.Name))
                End If
            Next
            ls.Add("}")
            ls.Add("</script>")
            Return New HtmlString(String.Join(vbCrLf, ls.ToArray()))
        End Function


        <System.Runtime.CompilerServices.Extension()> _
        Public Sub WriteSummaryBox_Begin(ByVal helper As HtmlHelper, ByVal title As String,
                                        Optional ByVal icon As String = "icon-user")
            Dim Writer = helper.ViewContext.Writer
            Writer.Write("<div class='sum-box'>" & vbCrLf)
            Writer.Write("          <div class='box-header well'>" & vbCrLf)
            Writer.Write("              <h2><i class='" & icon & "'></i>  " & title & "</h2>" & vbCrLf)
            Writer.Write("          </div>" & vbCrLf)
            Writer.Write("          <div class='box-content'>" & vbCrLf)
            Writer.Write("              <div class='box-content'>" & vbCrLf)
            Writer.Flush()
        End Sub
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub WriteSummaryBox_End(ByVal helper As HtmlHelper)
            Using Writer = helper.ViewContext.Writer
                Writer.Write("</div>" & vbCrLf)
                Writer.Write("</div>" & vbCrLf)
                Writer.Write("</div>" & vbCrLf)
                Writer.Flush()
            End Using


        End Sub

        <System.Runtime.CompilerServices.Extension()> _
        Public Function WritePopUpNipp(ByVal helper As HtmlHelper, ByVal nipp As String, ByVal nama As String) As HtmlString

            Return (helper.ActionLink(nama, "Detail", "Personalia",
                                           New With {.nipp = nipp},
                               New With {.class = "popoveritem", .data_nipp = nipp, .data_original_title = nama}))

        End Function

        <System.Runtime.CompilerServices.Extension()> _
        Public Function setActiveMenu(ByVal helper As HtmlHelper, ByVal action As String, ByVal controller As String) As HtmlString
            Dim currentController = helper.ViewContext.RouteData.Values("controller").ToString()
            Dim currentAction = helper.ViewContext.RouteData.Values("action").ToString()
            Dim rvalue As String = String.Empty
            If currentAction.Equals(action, StringComparison.CurrentCultureIgnoreCase) AndAlso
                currentController.Equals(controller, StringComparison.CurrentCultureIgnoreCase) Then
                rvalue = "class='active'"
            End If
            Return New HtmlString(rvalue)
        End Function


        <System.Runtime.CompilerServices.Extension()> _
        Public Function WriteDateValue(ByVal helper As HtmlHelper, ByVal datevalue As Date?,
                                       Optional ByVal format As String = "dd-MM-yyyy", Optional ByVal nulldata As String = "") As HtmlString
            If datevalue.HasValue AndAlso datevalue.Equals(Date.MinValue) = False Then
                Dim ci As CultureInfo = New CultureInfo("id-ID")

                Return New HtmlString(datevalue.Value.ToString(format, ci))
            Else
                Return New HtmlString(nulldata)
            End If
            Return New HtmlString(String.Empty)
        End Function

        <System.Runtime.CompilerServices.Extension()> _
        Public Function WriteCalculatedAge(ByVal helper As HtmlHelper, ByVal birthdate As Date) As Long
            Dim age As Long = DateDiff(DateInterval.Year, birthdate, Date.Today)
            Dim nextBirthday = DateSerial(Date.Today.Year, birthdate.Month, birthdate.Day)
            If nextBirthday.CompareTo(Date.Today) > 0 Then
                age -= 1
            End If
            Return age
        End Function

        Public Function GetStringDate(ByVal datevalue As Date?, Optional ByVal format As String = "dd-MM-yyyy") As String
            If datevalue.HasValue AndAlso datevalue.Equals(Date.MinValue) = False Then
                Return datevalue.Value.ToString(format)
            End If
            Return ""
        End Function

        <System.Runtime.CompilerServices.Extension()> _
        Public Function NavigationWithAuthorize(ByVal helper As HtmlHelper, ByVal label As String,
                                                ByVal action As String, ByVal controller As String,
                                                ByVal roles As String, Optional ByVal routevalue As Object = Nothing) As Boolean

            Dim result As Boolean = False
            Dim rolesarray = roles.Split(","c)
            Using Writer = helper.ViewContext.Writer
                If helper.ViewContext.HttpContext.User.Identity.IsAuthenticated Then
                    For Each role In rolesarray
                        If helper.ViewContext.HttpContext.User.IsInRole(role.Trim()) Or
                                              helper.ViewContext.HttpContext.User.IsInRole(role.Trim() & "_pada unit sendiri") Or
                                              helper.ViewContext.HttpContext.User.IsInRole(role.Trim() & "_Kirim hasil penilaian") Or
                                              helper.ViewContext.HttpContext.User.IsInRole("Administrator") Then
                            result = True
                            Exit For
                        End If
                    Next
                    If result Then
                        Writer.Write("<li>" & helper.ActionLink(label, action, controller, routevalue, New With {.class = "menulink"}).ToString() & "</li>" & vbCrLf)
                    End If
                Else
                    Writer.Write(String.Empty)
                End If
                Writer.Flush()
            End Using
            Return result
        End Function


        <System.Runtime.CompilerServices.Extension()> _
        Public Function NavigationWithOutAuthorize(ByVal helper As HtmlHelper, ByVal label As String,
                                                ByVal action As String, ByVal controller As String) As Boolean
            Using Writer = helper.ViewContext.Writer
                Writer.Write("<li>" & helper.ActionLink(label, action, controller, Nothing, New With {.class = "menulink"}).ToString() & "</li>" & vbCrLf)
                Writer.Flush()
            End Using
            Return True
        End Function

        <System.Runtime.CompilerServices.Extension()> _
        Public Sub WriteAccordionApplicantEnd(ByVal helper As HtmlHelper, ByVal ApplicationsID As String, ByVal action As String)

            Dim writer = helper.ViewContext.Writer
            writer.Write("</div>" & vbCrLf)

            writer.Write(helper.ActionLink("Tambah", action, New With {.ApplicationsID = ApplicationsID},
                                           New With {.class = "btn btn-info dropdown-toggle pull-right", .style = "margin: 5px 10px;"}))

            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)
            writer.Write("</div>" & vbCrLf)

        End Sub

    End Module
End Namespace


