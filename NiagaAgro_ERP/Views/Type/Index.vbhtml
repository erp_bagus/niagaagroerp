﻿@ModelType NiagaAgro_ERP.TblType
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@Imports NiagaAgro_ERP.HtmlHelpers
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
    <script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/Module/Type/index.js")"></script>
End Section
<div class="alert alert-info">
    <i class="glyphicon glyphicon-cog"></i>
    <strong>Konfigurasi Tipe Pembelian</strong>
</div>
<button class="btn btn-default btn-add" data-target="#modaltype" data-toggle="modal">
    <i class="glyphicon glyphicon-plus"></i>
</button>
<hr />
<table class="table table-bordered" id="tbltype">
    <thead>
        <tr class="success">
            <th>
                Type
            </th>
            <th>
            </th>
        </tr>
    </thead>
</table>
<!-- Modal -->
<div class="modal fade" id="modaltype" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Tambah Barang</h4>
            </div>
            <div class="modal-body">
                @Using Html.BeginForm("Create", "Type", Nothing, FormMethod.Post, New With {.class = "form-horizontal", .id = "form-type"})
                    @Html.ValidationSummary(True)
                    @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Tipe
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBox("Type", Nothing, New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.Type)
                        </div>
                    </div>
                    @<center>
                        <button class="btn btn-primary" type="submit" id="btn-submit">
                            Save</button>
                        &nbsp;
                        <button class="btn btn-default" type="reset" id="btn-reset">
                            Reset</button>
                    </center>
                End Using
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close</button>
            </div>
        </div>
    </div>
</div>
@Using Html.LoadingHelpers(Url.Content("~/Content/img/progress.gif"))
End Using
