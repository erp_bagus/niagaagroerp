﻿@ModelType IEnumerable(Of NiagaAgro_ERP.TblPenjualan)
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@Imports NiagaAgro_ERP.HtmlHelpers
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment-with-locales.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/autonumeric/autoNumeric.js")"></script>
<script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/accounting.min.js")"></script>
@*<script type="text/javascript" src="@Url.Content("~/Scripts/Module/Sale/index.js")"></script>*@
<script type="text/javascript" src="@Url.Content("~/Scripts/Module/Sale/Sale.js")"></script>
End Section
<div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i><i class="glyphicon glyphicon-tasks"></i>
    <strong>Informasi Penjualan</strong>
</div>
<a href="/Sale/Create" class="btn btn-default btn-add">
    <i class="glyphicon glyphicon-plus"></i>
</a>
<hr />
<table class="table table-bordered" id="tblsale">
    <thead>
        <tr class="success">
            <th>
               Form Number
            </th>
            <th>
                Nama Pelanggan
            </th>
            <th>
                Tanggal 
            </th>
            <th>
                Tipe
            </th>
            <th>
            </th>
        </tr>
    </thead>
    <tbody id="tbodydata">
            @Code Dim ctx As New NiagaAgro_ERP.NiagaAgroEntities
                If Model.Count <> 0 Then
                    For Each m In Model
                        Dim name = (From x In ctx.TblCustomers Where x.ID = m.IDCustomer Select x.Nama).FirstOrDefault()
            End Code
                    <tr><td> @m.FormNumber</td>
                        <td> @name</td>
                        <td> @m.Tanggal.ToString("dd MMMM yyyy")</td>
                        <td> @m.TblType.Type</td>
                        <td> <a href='/Sale/Edit/@m.ID' class='btn btn-primary edit-data' rel='tooltip' title='Edit' data-placement='left'><i class='glyphicon glyphicon-pencil'></i></a>
                             @Code If (m.TblType.Type = "Bon") Then End Code <a href='/Sale/Paid/@m.ID' class='btn btn-primary edit-data' rel='tooltip' title='Pelunasan' data-placement='left'><i class='glyphicon glyphicon-check'></i></a> @Code End If End Code
                            @*<a href='#' onclick='return delpenjualan(@m.ID)' class='btn btn-danger btn-del' rel='tooltip' title='Delete' data-placement='right'><i class='glyphicon glyphicon-remove'></i></a>*@
                            <a href='/Sale/PrintKwitansi/@m.ID' class='btn btn-primary edit-data' rel='tooltip' title='Cetak Kwitansi' data-placement='left'><i class='glyphicon glyphicon-print'></i></a>
                        </td>
                    </tr> 
                @Code Next
                  Else End Code
                      <tr><td colspan="5"><center>No Data</center></td></tr>
                  @Code End If End Code
                     
   </tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="modalsale" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Tambah Barang</h4>
            </div>
            <div class="modal-body">
                @Code
                Using Html.BeginForm("Create", "Sale", Nothing, FormMethod.Post, New With {.id = "formsale"})
                @Html.ValidationSummary(True)
                End Code
                <div id="alertplaceholder" style="width: 300px; margin: 0 auto; color:white;" class="label-danger"></div>
                <div class="form-horizontal">
                    <div class="form-group">
                    <label class="col-xs-2 control-label">No Form :</label>
                    <div class="col-xs-2">
                        @Html.TextBox("FormNumber", ViewData("FormNumber"), New With {.class = "form-control", .readonly = "readonly", .style = "margin-bottom:-50px"})
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-2 control-label">Tanggal Transaksi :</label>
                    <div class="col-xs-2">
                        @Html.TextBox("Tanggal", Date.Now.ToString("dd MMMM yyyy"), New With {.class = "form-control", .readonly = "readonly", .style = "margin-bottom:-50px"})
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-2 control-label">Nama :</label>
                    <div class="col-xs-3">
                        @Html.DropDownList("IDCustomer", New SelectList(DirectCast(ViewData("IDCustomer"), IEnumerable), "ID", "Nama"), "", New With {.class = "form-control idcustomer", .style = "margin-bottom:-50px"})
                    </div>
                    </div>
                     <div class="form-group">
                    <label class="col-xs-2 control-label">Tipe Pembayaran :</label>
                    <div class="col-xs-2">
                        @Html.DropDownList("Type", New SelectList(DirectCast(ViewData("Type"), IEnumerable), "ID", "Type"), New With {.class = "form-control"})
                    </div>
                    </div>
                </div>
                <div>
                    <a href="javascript:void(0)" class="btn btn-success button" id="AddItem" style="margin-left:70px">
                    <i class="icon-plus-sign icon-white"></i>&nbsp;Tambah</a>
                    <br />
                    </div>
                    <center>
                    <table id="tableSale" class="table table-hover table-condensed table-bordered table-striped" style="width:900px;" >
                        <thead class = "orangeheader">
                            <tr>
                                <th>#</th>
                                <th>Nama Barang</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Keterangan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id = "List">
                            <tr id='0'>
                            <td class='td-no'><span></span><input type='hidden' id='ID_0' name='[0].ID' value='' /><input type='hidden' id='Tanggal_0' name='[0].Tanggal' value='@Date.Now.ToString("dd/MM/yyyy")' /></td>
                            <td class='td-idbarang'>@Html.DropDownList("[0].IDBarang", New SelectList(DirectCast(ViewData("IDBarang"), IEnumerable), "IDBarang", "NamaBrg"), "--Select Barang--", New With {.class = "form-control"})</td>
                            <td class='td-jumlah'><input type='text' id='Jumlah_0' name='[0].Jumlah' style='width:100%;' class='form-control jumlah' /></td>
                            <td class='td-hargajual'><input type='text' name='[0].TempHarga' id='TempHarga_0' style='width:100%;' class='form-control tempharga' /><input type='hidden' name='[0].HargaJual' id='HargaJual_0' style='width:100%;' class='form-control harga' /></td>
                            <td class='td-keterangan'><input type='text' name='[0].Keterangan' id='Keterangan_0' style='width:100%;' class='form-control' /></td>
                            <td class='td-action'></td>
                            </tr>
                        </tbody>
                         <tr><td colspan="6"><input type="hidden" id="tempidbarang" class="tempidbarang"/></td></tr>                    
                    </table>
                     </center>
                      <div class="form-horizontal">
                    <div class="form-group">
                    <label class="col-xs-8 control-label">Total Belanja :</label>
                    <div class="col-xs-3">
                        @Html.TextBox("TotalBelanja", Nothing, New With {.class = "form-control total", .style = "margin-bottom:-50px;font-size:20px", .readonly = "readonly"})
                    </div>
                    </div>
                     <div class="form-group">
                    <label class="col-xs-8 control-label">
                        Bayar :
                    </label>
                    <div class="col-xs-3">
                        @Html.TextBox("Bayar", Nothing, New With {.class = "form-control bayar", .style = "margin-bottom:-50px;font-size:20px"})
                    </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-8 control-label">
                            Kembalian :
                        </label>
                        <div class="col-xs-3">
                           @Html.TextBox("Kembalian", Nothing, New With {.class = "form-control", .style = "font-size:20px", .readonly = "readonly"})
                        </div>
                    </div>
                   </div>
                   <center>
                   <div class="modal-footer">
                    <button class="btn btn-primary" type="submit" id="btn-submit" class="enableOnInput" onclick="return save();" />
                        Simpan</button>
                    &nbsp;
                    <button class="btn btn-default" type="reset" id="btn-reset">
                        Reset</button>
                    &nbsp; <a class="btn btn-success" href="@Url.Action("Index", "Sale")">Batal
                    </a>
                    </div>
                    </center>

                   
                @Code
                End Using
                End Code
                 </div>
            </div>
        </div>
    </div>
   @* @Html.Partial("Edit")*@
@Using Html.LoadingHelpers(Url.Content("~/Content/img/progress.gif"))
End Using