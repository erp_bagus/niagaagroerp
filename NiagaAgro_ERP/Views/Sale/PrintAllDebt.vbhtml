﻿@ModelType NiagaAgro_ERP.TblPenjualan
@Code
    ViewData("Title") = "Print Pembayaran Utang Semua Pelanggan"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@Imports NiagaAgro_ERP.HtmlHelpers
<div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i><i class="glyphicon glyphicon-tasks"></i>
    <strong>Laporan Pembayaran Utang Semua Pelanggan</strong>
</div>
@Code
    Using Html.BeginForm("PrintAllDebt", "Sale", Nothing, FormMethod.Post, New With {.id = "formprintdebt"})
    @Html.ValidationSummary(True)
End Code
 <div class="form-horizontal" style="height:500px;">
        <div class="form-group">
        <label class="col-xs-2 control-label">
            Tanggal Awal :
        </label>
        <div class="col-xs-2">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar"></i>
                </div>
                @Html.TextBox("tglawal", Date.Now.ToString("dd/MM/yyyy"), New With {.class = "form-control"})
            </div>
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">
            Tanggal Akhir :
        </label>
        <div class="col-xs-2">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar"></i>
                </div>
                @Html.TextBox("tglakhir", Date.Now, New With {.class = "form-control"})
            </div>
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Tipe Pembayaran :</label>
        <div class="col-xs-2">
            @Html.DropDownList("Type","")
        </div>
        </div>
     <center>
    <div style="margin-right:55%;">
        <button class="btn btn-primary" type="submit" id="btn-submit" class="enableOnInput" />
            Cetak</button>
        &nbsp; <a class="btn btn-success" href="@Url.Action("Index", "Sale")">Kembali
        </a>
     </div>
     </center>
     </div>
@Code
    End Using
End Code
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.min.css")" />
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/select2-3.3.2/select2.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment-with-locales.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/id.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.js")"></script>
<script src="@Url.Content("~/Content/select2-3.3.2/select2.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/autonumeric/autoNumeric.js")"></script>
<script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#Type").addClass("form-control");
        $('#tgl').datetimepicker({
            format: 'DD-MM-YYYY'
        });
        $('#tglawal').datetimepicker({
            format: 'DD-MM-YYYY'
        });
        $('#tglakhir').datetimepicker({
            format: 'DD-MM-YYYY'
        });
    });

</script>
End Section
@Using Html.LoadingHelpers(Url.Content("~/Content/img/progress.gif"))
End Using