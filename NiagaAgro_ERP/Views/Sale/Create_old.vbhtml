﻿@ModelType NiagaAgro_ERP.ModelSale
@Code
    ViewData("Title") = "Create"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.min.css")" />
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment-with-locales.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.js")"></script>
<script src="@Url.Content("~/Content/select2-3.3.2/select2.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/autonumeric/autoNumeric.js")"></script>
<script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/formValidation.min.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/Module/Sale/Create.js")"></script>
@code
    Using Html.BeginForm("Create", "Sale", Nothing, FormMethod.Post, New With {.id = "form-sale"})
    @Html.ValidationSummary(True)
End Code
<div class="col-sm-3 col-sm-offset-1">
    <button class="btn btn-default btn-add" rel="tooltip" data-placement="right" title="Tambah">
        <i class="glyphicon glyphicon-plus"></i>
    </button>
</div>
<div class="row">
    <div class="col-sm-9 col-sm-offset-1">
        <table class="table table-bordered">
            <thead>
                <tr class="warning">
                    <th>
                        Nama Barang
                    </th>
                    <th>
                        Jumlah
                    </th>
                    <th>
                        Harga
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        @Html.DropDownList("IDBarang", New SelectList(DirectCast(ViewData("Barang"), IEnumerable), "IDBarang", "NamaBrg"), "Barang", New With {.class = "form-control"})
                        @*@Html.ValidationMessageFor(Function(model) model.IDBarang)*@
                    </td>
                    <td>
                        @Html.TextBox("Jumlah", Nothing, New With {.class = "form-control"})
                        @*@Html.ValidationMessageFor(Function(model) model.Jumlah)*@
                    </td>
                    <td>
                        @Html.TextBox("HargaJual", Nothing, New With {.class = "form-control auto harga"})
                        @*@Html.ValidationMessageFor(Function(model) model.HargaJual)*@
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div id="cloning" style="display: none;">
    <div clss="row">
        <div class="col-sm-9 col-sm-offset-1">
            <table class="table table-bordered">
                <tr>
                    <td colspan="4">
                        <button class="btn-default btn-xs btn-add" rel="tooltip" data-placement="right" title="Tambah">
                            <i class="glyphicon glyphicon-plus"></i>
                        </button>
                        <button class="btn-default btn-xs btn-remove pull-right" rel="tooltip" data-placement="right" title="Hapus">
                            <i class="glyphicon glyphicon-remove "></i>
                        </button>
                    </td>
                </tr>
                <tbody>
                    <tr>
                        <td>
                            @Html.DropDownList("IDBarang", New SelectList(DirectCast(ViewData("Barang"), IEnumerable), "IDBarang", "NamaBrg"), "Barang", New With {.class = "form-control"})
                            @*@Html.ValidationMessageFor(Function(model) model.IDBarang)*@
                        </td>
                        <td>
                            @Html.TextBox("Jumlah", Nothing, New With {.class = "form-control"})
                            @*@Html.ValidationMessageFor(Function(model) model.Jumlah)*@
                        </td>
                        <td>
                            @Html.TextBox("HargaJual", Nothing, New With {.class = "form-control auto harga"})
                            @*@Html.ValidationMessageFor(Function(model) model.HargaJual)*@
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-3 control-label">
            Tanggal
        </label>
        <div class="col-sm-2">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar"></i>
                </div>
                @Html.TextBox("Tanggal", Now, New With {.class = "form-control"})
                @Html.ValidationMessageFor(Function(model) model.Tanggal)
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">
            Total Belanja
        </label>
        <div class="col-sm-3">
            @Html.TextBox("TotalBelanja", Nothing, New With {.class = "form-control auto"})
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">
            Bayar
        </label>
        <div class="col-sm-3">
            @Html.TextBox("Bayar", Nothing, New With {.class = "form-control auto"})
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">
            Kembalian
        </label>
        <div class="col-sm-3">
            @Html.TextBox("Kembalian", Nothing, New With {.class = "form-control auto"})
        </div>
    </div>
    <div class="form-group" id="addDp">
        <label class="col-sm-3 control-label">
            Tipe
        </label>
        <div class="col-sm-2">
            @Html.DropDownList("Type", New SelectList(DirectCast(ViewData("Tipe"), IEnumerable), "ID", "Type"), "Tipe", New With {.class = "form-control"})
            @Html.ValidationMessageFor(Function(model) model.Type)
        </div>
    </div>
    <div class="form-group" id="dp" style="display: none;">
        <label class="col-sm-3 control-label">
            Uang Panjar
        </label>
        <div class="col-sm-3">
            @Html.TextBox("UangPanjar", Nothing, New With {.class = "form-control auto"})
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">
            Nama Pelanggan
        </label>
        <div class="col-sm-3">
            @Html.TextBox("Nama", Nothing, New With {.class = "form-control"})
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">
            Keterangan
        </label>
        <div class="col-sm-3">
            @Html.TextArea("Keterangan", New With {.class = "form-control"})
        </div>
    </div>
    <center>
        <button class="btn btn-primary" type="submit" id="btn-submit">
            Save</button>
        &nbsp;
        <button class="btn btn-default" type="reset" id="btn-reset">
            Reset</button>
        &nbsp; <a class="btn btn-success" href="@Url.Action("Index", "Sale")">Cancel
        </a>
    </center>
</div>
@Code
    End Using
End Code
