﻿@ModelType NiagaAgro_ERP.ModelPaid
@Imports System.Globalization

@Code
    ViewData("Title") = "Pelunasan"
    Layout = "~/Views/Shared/_Layout.vbhtml"
    Dim detail As List(Of NiagaAgro_ERP.TblPenjualanDetail) = ViewData("listsale")
    Dim data As List(Of NiagaAgro_ERP.TblPenjualan) = ViewData("data")
    Dim paidlist As List(Of NiagaAgro_ERP.TblPelunasan) = ViewData("paidlist")
    Using Html.BeginForm("SavePaid", "Sale", Nothing, FormMethod.Post, New With {.id = "formsale"})
    @Html.ValidationSummary(True) 
      
    If data IsNot Nothing Then
        For Each m In data
    End Code
    <div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i><i class="glyphicon glyphicon-tasks"></i>
    <strong>Tambah Pelunasan</strong>
</div>
    <div id="alertplaceholder" style="width: 300px; margin: 0 auto; color:white;" class="label-danger"></div>
    <div class="form-horizontal">
        <div class="form-group">
        <label class="col-xs-2 control-label">No Form :</label>
        <div class="col-xs-2">
            @Html.Hidden("IDPenjualan", m.ID)
            @m.FormNumber
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Tanggal Transaksi :</label>
        <div class="col-xs-2">
            @m.Tanggal.ToString("dd MMMM yyyy")
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Nama :</label>
        <div class="col-xs-3">
            @m.TblCustomer.Nama
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Tipe Pembayaran :</label>
        <div class="col-xs-2">
            @m.TblType.Type
        </div>
        </div>
        <div class="form-group" id="duedate">
        <label class="col-xs-2 control-label">
            Tanggal Jatuh Tempo :
        </label>
        <div class="col-xs-2">
            @code If (Not IsNothing(m.DueDate)) Then End Code
            @m.DueDate.Value.ToString("dd-MM-yyyy")
            @Code End If End Code
        </div>
        </div>
        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
                var type = '@m.TblType.Type';
                var bayar = '@m.Buy';
                if (type == "Bon") {
                    $('#duedate').show();

                }
                $('.tempbayar').html(accounting.formatMoney(bayar.replace(",0000"), "Rp. ", 2, ".", ","));
                
            });
           
        </script>
    </div>
    @Code Next
                 End If End Code
     <center>
        <table class="table table-hover table-condensed table-bordered table-striped " style="width:90%;" >
            <thead class = "orangeheader">
                <tr>
                    <th>#</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Total Harga</th>
                    <th>Keterangan</th>
                    </tr>
            </thead>
            <tbody id = "List">
            @Code
                    Dim a As Integer = 0
                    Dim b As Integer = 0
                    Dim number As Integer = 1
                    If detail IsNot Nothing Then
                        For Each i In detail
                            Dim strNumber = String.Empty
                            strNumber = number.ToString
                            Dim totalharga = i.Jumlah * i.HargaJual
            End Code
                <tr id='@a'>
                <td class='td-no'><span>@strNumber</span><input type='hidden' id='ID_"@a"' name='[@a].ID' value='' /><input type='hidden' id='Tanggal_"@a"' name='[@a].Tanggal' value='@i.Tanggal.ToString("dd/MM/yyyy")' /></td>
                <td class='td-idbarang'>@i.MasterBarang.NamaBrg</td>
                <td class='td-jumlah' style='width:30px;'><span id='Jumlah_"@a"' class='jumlah'>@i.Jumlah</span></td>
                <td class='td-hargajual'><span id='TempHarga_"@a"' style='width:100%;' class='tempharga'>@i.HargaJual</span><input type='hidden' name='[@a].HargaJual' id='HargaJual_"@a"' style='width:100%;' class='form-control harga' value="@i.HargaJual"/></td>
                 <td class='td-totalHarga'><span class='temptotalharga'>@totalharga</span><input type='hidden' name='[@a].HargaJual' id='TotalHarga_"@a"' class='form-control totalharga' value="@totalharga"/></td>
                <td class='td-keterangan'><span id='Keterangan_"@a"' style='width:100%;'>@i.Keterangan</span></td>
                </tr> 
           @Code
                           a += 1
                       Next
                  End If
                
           End Code
            </tbody>
                <tr><td colspan="6"><input type="hidden" id="tempidbarang" class="tempidbarang"/><input type="hidden" id="tempArrJumlah" /></td></tr>                    
        </table>
            </center>
        <div class="form-horizontal">
            <div class="form-group">
            <label class="col-xs-8 control-label">Total Belanja :</label>
            <div class="col-xs-3">
                <span class = "jumlahhargabelanja" style = "margin-bottom:-50px;font-size:20px"></span>
                <input type = "hidden" name ="jumlahhargabelanja" id="jumlahhargabelanja" />
            </div>
            </div>
            <div class="form-group">
            <label class="col-xs-8 control-label">
                Bayar :
            </label>
            <div class="col-xs-3">
                <span id="tempbuy" class="tempbayar" style = "margin-bottom:-50px;font-size:20px"></span>
              </div>
            </div>
        </div>
        <br />
        <table class="table table-hover table-condensed table-bordered table-striped " style="width:40%;margin-left:5%;" >
            <thead class = "orangeheader">
                <tr>
                    <th>#</th>
                    <th>Tgl. Pelunasan</th>
                    <th>Jumlah Bayar</th>
                    <th>Sisa Cicilan</th>
                    </tr>
            </thead>
            <tbody>
            @Code
                    Dim ci As New CultureInfo("id-ID")
                    Dim x As Integer = 1
                    Dim list = paidlist.Count
                    If list <> 0 Then
                        For Each p In paidlist
                            x = x.ToString()
                           
            End Code
                    <tr>
                    <td><span>@x</span></td>
                    <td style="text-align:center;">@p.Tanggal.ToString("dd/MM/yyyy")</td>
                    <td style="width:200px;">@p.Bayar.ToString("C", (ci))</td>
                    <td>@p.Sisa.ToString("C", (ci))</td>
                    </tr> 
           @Code
                           x += 1
                       Next
                   Else End Code
                    <tr><td colspan='4'><center>Tidak ada Angsuran</center></td></tr>
           @Code        End If
                   
                
           End Code
            </tbody>                   
        </table>
       <div class="form-horizontal">
            <div class="form-group">
            <label class="col-xs-2 control-label">
                Tanggal Angsuran :
            </label>
            <div class="col-xs-2">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                    </div>
                    <input type="hidden" id="maxbayar" value="@ViewData("maxbayar")" />
                    @Html.TextBoxFor(Function(model) model.Tanggal, New With {.class = "form-control tgllunas"})
                    @Html.ValidationMessageFor(Function(model) model.Tanggal)
                </div>
            </div>
            </div>
            <div class="form-group">
            <label class="col-xs-2 control-label">Angsuran :</label>
            <div class="col-xs-3">
                @Html.HiddenFor(Function(model) model.Bayar, New With {.class = "form-control paid", .style = "font-size:20px"})
                @Html.TextBox("dtbayar", Nothing, New With {.class = "form-control dtpaid", .style = "font-size:20px"})
                @Html.ValidationMessageFor(Function(model) model.Bayar)
            </div>
            </div>
            <div class="form-group">
            <label class="col-xs-2 control-label">Sisa Pembayaran :</label>
            <div class="col-xs-3">
                @Html.HiddenFor(Function(model) model.Sisa, New With {.class = "form-control sisa", .style = "font-size:20px"})
                @Html.TextBox("dtSisa", Nothing, New With {.class = "form-control dtsisa", .style = "font-size:20px", .readonly = "readonly"})
                @Html.ValidationMessageFor(Function(model) model.Sisa)
            </div>
            </div>
        </div>
        <center>
        <div class="modal-footer">
        <button class="btn btn-primary" type="submit" id="btn-submit" class="enableOnInput" onclick="return confirm('Apakah data akan disimpan?');"/>
            Simpan</button>
        &nbsp;
        <button class="btn btn-default" type="reset" id="btn-reset">
            Reset</button>
        &nbsp; <a class="btn btn-success" href="@Url.Action("Index", "Sale")">Batal
        </a>
        </div>
        </center>
    @Code
    End Using
    End Code
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment-with-locales.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/id.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/autonumeric/autoNumeric.js")"></script>
<script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/accounting.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/Module/Sale/Sale.js")"></script>
End Section