﻿@ModelType NiagaAgro_ERP.ModelSale
@Code
    ViewData("Title") = "Create"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@Code
    Using Html.BeginForm("Save", "Sale", Nothing, FormMethod.Post, New With {.id = "formsale"})
    @Html.ValidationSummary(True)
End Code

<div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i><i class="glyphicon glyphicon-tasks"></i>
    <strong>Tambah Penjualan</strong>
</div>
 <div id="alertplaceholder" style="width: 300px; margin: 0 auto; color:white;" class="label-danger"></div>
<div class="form-horizontal">
        <div class="form-group">
        <label class="col-xs-2 control-label">No Form :</label>
        <div class="col-xs-2">
            <input type="hidden" name="ID" class="ID" value="" />
            @Html.TextBox("FormNumber", ViewData("FormNumber"), New With {.class = "form-control", .readonly = "readonly", .style = "margin-bottom:-50px"})
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Tanggal Transaksi :</label>
        <div class="col-xs-2">
            @Html.TextBox("Tanggal",  Date.Now.ToString("dd MMMM yyyy"), New With {.class = "form-control", .readonly = "readonly", .style = "margin-bottom:-50px"})
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Nama :</label>
        <div class="col-xs-3">
            @Html.DropDownList("IDCustomer", New SelectList(DirectCast(ViewData("IDCustomer"), IEnumerable), "ID", "Nama"), "", New With {.class = "form-control", .style = "margin-bottom:-50px"})
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Tipe Pembayaran :</label>
        <div class="col-xs-2">
            @Html.DropDownList("Type", New SelectList(DirectCast(ViewData("Type"), IEnumerable), "ID", "Type"), New With {.class = "form-control", .id = "type"})
        </div>
        </div>
        <div class="form-group" id="duedate">
        <label class="col-xs-2 control-label">
            Tanggal Jatuh Tempo :
        </label>
        <div class="col-xs-2">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar"></i>
                </div>
                @Html.TextBox("DueDate", Nothing, New With {.class = "form-control duedate"})
            </div>
        </div>
        </div>
    </div>
    <div>
        <a href="javascript:void(0)" class="btn btn-success button" id="AddItem" style="margin-left:70px">
        <i class="icon-plus-sign icon-white"></i>&nbsp;Tambah</a>
        <br />
        </div>
        <center>
        <table id="barang" class="table table-hover table-condensed table-bordered table-striped " style="width:90%;" >
            <thead class = "orangeheader">
                <tr>
                    <th>#</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Keterangan</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id = "List">
                <tr id='0'>
                <td class='td-no'><span>0</span><input type='hidden' id='ID_0' name='[0].ID' value='' /><input type='hidden' id='Tanggal_0' name='[0].Tanggal' value='@Date.Now.ToString("dd/MM/yyyy")' /></td>
                <td class='td-idbarang'>@Html.DropDownList("[0].IDBarang", New SelectList(DirectCast(ViewData("IDBarang"), IEnumerable), "IDBarang", "NamaBrg"), "--Select Barang--", New With {.class = "form-control"})</td>
                <td class='td-jumlah' style="width:30px;"><input type='text' id='Jumlah_0' name='[0].Jumlah' style='width:100%;' class='form-control jumlah' /></td>
                <td class='td-hargajual'><input type='text' name='[0].TempHarga' id='TempHarga_0' style='width:100%;' class='form-control tempharga' /><input type='hidden' name='[0].HargaJual' id='HargaJual_0' style='width:100%;' class='form-control harga' /></td>
                <td class='td-keterangan'><input type='text' name='[0].Keterangan' id='Keterangan_0' style='width:100%;' class='form-control' /></td>
                <td class='td-action'></td>
                </tr>
            </tbody>
                <tr><td colspan="6"><input type="hidden" id="tempidbarang" class="tempidbarang"/><input type="hidden" id="tempArrJumlah" /></td></tr>                    
        </table>
            </center>
            <div class="form-horizontal">
        <div class="form-group">
        <label class="col-xs-8 control-label">Total Belanja :</label>
        <div class="col-xs-3">
            @Html.TextBox("TotalBelanja", Nothing, New With {.class = "form-control total", .style = "margin-bottom:-50px;font-size:20px", .readonly = "readonly"})
            @Html.Hidden("TempTotalBelanja",Nothing, New With {.class = "form-control temptotalbelanja"})
        </div>
        </div>
            <div class="form-group">
        <label class="col-xs-8 control-label">
            Bayar :
        </label>
        <div class="col-xs-3">
              @Html.TextBox("TempBuy", Nothing, New With {.class = "form-control tempbayar", .style = "margin-bottom:-50px;font-size:20px"}) 
             @Html.Hidden("Buy", Nothing, New With {.class = "form-control bayar"})
        </div>
        </div>
        <div class="form-group">
            <label class="col-xs-8 control-label">
                Kembalian :
            </label>
            <div class="col-xs-3">
                @Html.TextBox("Kembalian", Nothing, New With {.class = "form-control", .style = "font-size:20px", .readonly = "readonly"})
            </div>
        </div>
        </div>
        <center>
        <div class="modal-footer">
        <button class="btn btn-primary" type="submit" id="btn-submit" class="enableOnInput" onclick="return save();" />
            Simpan</button>
        &nbsp;
        <button class="btn btn-default" type="reset" id="btn-reset">
            Reset</button>
        &nbsp; <a class="btn btn-success" href="@Url.Action("Index", "Sale")">Batal
        </a>
        </div>
        </center>
@Code
    End Using
End Code
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment-with-locales.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/id.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.js")"></script>
<script src="@Url.Content("~/Content/select2-3.3.2/select2.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/autonumeric/autoNumeric.js")"></script>
<script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/plugin/formvalidation/formValidation.min.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/accounting.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/Module/Sale/Sale.js")"></script>
End Section
