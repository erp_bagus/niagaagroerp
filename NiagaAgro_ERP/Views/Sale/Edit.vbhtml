﻿@ModelType NiagaAgro_ERP.TblPenjualan
@Code
    ViewData("Title") = "Edit"
    Layout = "~/Views/Shared/_Layout.vbhtml"
    Using Html.BeginForm("Save", "Sale", Nothing, FormMethod.Post, New With {.id = "formsale"})
    @Html.ValidationSummary(True)
            Dim detail As List(Of NiagaAgro_ERP.TblPenjualanDetail) = ViewData("listsale")
    End Code
    <div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i><i class="glyphicon glyphicon-tasks"></i>
    <strong>Ubah Penjualan</strong>
</div>
    <div id="alertplaceholder" style="width: 300px; margin: 0 auto; color:white;" class="label-danger"></div>
    <div class="form-horizontal">
        <div class="form-group">
        <label class="col-xs-2 control-label">No Form :</label>
        <div class="col-xs-2">
            @Html.Hidden("ID")
            @Html.TextBox("FormNumber", Model.FormNumber, New With {.class = "form-control", .readonly = "readonly", .style = "margin-bottom:-50px"})
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Tanggal Transaksi :</label>
        <div class="col-xs-2">
            @Html.TextBox("Tanggal", Model.Tanggal.ToString("dd MMMM yyyy"), New With {.class = "form-control", .readonly = "readonly", .style = "margin-bottom:-50px"})
        </div>
        </div>
        <div class="form-group">
        <label class="col-xs-2 control-label">Nama :</label>
        <div class="col-xs-3">
            @Html.DropDownList("IDCustomer", "")
        </div>
        </div>
            <div class="form-group">
        <label class="col-xs-2 control-label">Tipe Pembayaran :</label>
        <div class="col-xs-2">
            @Html.DropDownList("Type")
        </div>
        </div>
        <div class="form-group" id="duedate">
        <label class="col-xs-2 control-label">
            Tanggal Jatuh Tempo :
        </label>
        <div class="col-xs-2">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar"></i>
                </div>
                 @Code If (IsNothing(Model.DueDate))  Then End Code 
                @Html.TextBox("DueDate", Nothing, New With {.class = "form-control duedate"})
                @Code Else End Code  
                 @Html.TextBox("DueDate", Model.DueDate.Value.ToString("dd/MM/yyyy"), New With {.class = "form-control duedate"}) 
                @Code  End If
                End Code
                
            </div>
        </div>
        </div>
    </div>
    <div>
        <a href="javascript:void(0)" class="btn btn-success button" id="AddItem" style="margin-left:70px">
        <i class="icon-plus-sign icon-white"></i>&nbsp;Tambah</a>
        <br />
        </div>
        <center>
        <table id="barang" class="table table-hover table-condensed table-bordered table-striped" style="width:90%;" >
            <thead class = "orangeheader">
                <tr>
                    <th>#</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Keterangan</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id = "List">
            @Code
                    Dim a As Integer = 0
                    Dim b As Integer = 0
                    Dim number As Integer = 1
                    If detail IsNot Nothing Then
                        For Each i In detail
                            Dim strNumber = String.Empty
                            strNumber = number.ToString
            End Code
                <tr id='@a'>
                <td class='td-no'><span>@strNumber</span><input type='hidden' id='ID_"@a"' name='[@a].ID' value='' /><input type='hidden' id='Tanggal_"@a"' name='[@a].Tanggal' value='@i.Tanggal.ToString("dd/MM/yyyy")' /></td>
                <td class='td-idbarang'>@Html.DropDownList("["& a &"].IDBarang", New SelectList(DirectCast(ViewData("IDBarang"), IEnumerable), "IDBarang", "NamaBrg"), "--Select Barang--", New With {.class = "form-control"})</td>
                     <script type="text/javascript">
                         $(".td-idbarang select[name*='[@a].IDBarang'] ").find('option[value*="@i.IDBarang"]').attr("selected", "selected");
                     </script>
                <td class='td-jumlah' style="width:30px;"><input type='text' id='Jumlah_"@a"' name='[@a].Jumlah' style='width:100%;' class='form-control jumlah' value="@i.Jumlah"/></td>
                <td class='td-hargajual'><input type='text' name='[@a].TempHarga' id='TempHarga_"@a"' style='width:100%;' class='form-control tempharga' value="@i.HargaJual" /><input type='hidden' name='[@a].HargaJual' id='HargaJual_"@a"' style='width:100%;' class='form-control harga' value="@i.HargaJual"/></td>
                <td class='td-keterangan'><input type='text' name='[@a].Keterangan' id='Keterangan_"@a"' value="@i.Keterangan" style='width:100%;' class='form-control' /></td>
                <td class='td-action'><a data-id = '@a' onclick='removerow(this);' class='glyphicon glyphicon-remove'  id='move' title='batal'></a></td>
                    <script type="text/javascript">
                        if ("@a" == 0) { $('#move').removeAttr('class'); }
                    </script>
                </tr> 
           @Code
                           a += 1
                       Next
                  End If
                
           End Code
            </tbody>
                <tr><td colspan="6"><input type="hidden" id="tempidbarang" class="tempidbarang"/><input type="hidden" id="tempArrJumlah" /></td></tr>                    
        </table>
            </center>
            <div class="form-horizontal">
        <div class="form-group">
        <label class="col-xs-8 control-label">Total Belanja :</label>
        <div class="col-xs-3">
            @Html.TextBox("TotalBelanja", Nothing, New With {.class = "form-control total", .style = "margin-bottom:-50px;font-size:20px", .readonly = "readonly"})
            @Html.Hidden("TempTotalBelanja",Nothing, New With {.class = "form-control temptotalbelanja"})
        </div>
        </div>
            <div class="form-group">
        <label class="col-xs-8 control-label">
            Bayar :
        </label>
        <div class="col-xs-3">
            @Html.TextBox("TempBuy", Model.Buy, New With {.class = "form-control tempbayar", .style = "margin-bottom:-50px;font-size:20px"}) 
             @Html.Hidden("Buy", Model.Buy, New With {.class = "form-control bayar"})
        </div>
        </div>
        <div class="form-group">
            <label class="col-xs-8 control-label">
                Kembalian :
            </label>
            <div class="col-xs-3">
                @Html.TextBox("Kembalian", Nothing, New With {.class = "form-control", .style = "font-size:20px", .readonly = "readonly"})
            </div>
        </div>
        </div>
        <center>
        <div class="modal-footer">
        <button class="btn btn-primary" type="submit" id="btn-submit" class="enableOnInput" onclick="return save();" />
            Simpan</button>
        &nbsp;
        <button class="btn btn-default" type="reset" id="btn-reset">
            Reset</button>
        &nbsp; <a class="btn btn-success" href="@Url.Action("Index", "Sale")">Batal
        </a>
        </div>
        </center>
    @Code
    End Using
    End Code
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment-with-locales.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/id.js")"></Script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/autonumeric/autoNumeric.js")"></script>
<script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/accounting.min.js")"></script>
<script type="text/javascript" src="@Url.Content("~/Scripts/Module/Sale/Sale.js")"></script>
End Section