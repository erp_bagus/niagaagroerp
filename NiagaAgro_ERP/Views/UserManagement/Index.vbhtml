﻿@ModelType NiagaAgro_ERP.UserViewModel
@Code
    ViewData("Title") = "User Management"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
  <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment.min.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment-with-locales.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.js")"></script>
     <script src="@Url.Content("~/Content/select2-3.3.2/select2.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/Module/UserManagement/index.js")"></script>
End Section
<div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i> <i class="glyphicon glyphicon-tasks"></i>
    <strong>Informasi User</strong>
</div>
<button class="btn btn-default btn-add" data-target="#modaluser" data-toggle="modal">
    <i class="glyphicon glyphicon-plus"></i>
</button>
<hr />
<table class="table table-bordered" id="tblUser">
    <thead>
        <tr class="success">
            <th>
                Nama 
            </th>
            <th>
                Role
            </th>
            <th style="width:10%;">
            </th>
        </tr>
    </thead>
     <tbody>
            @Code 
                Dim data As List(Of NiagaAgro_ERP.UserViewModel) = ViewData("listmodel")
                If data IsNot Nothing Then
                    For Each d In data
                       
            End Code
                    <tr><td> @d.UserName</td>
                        <td> @d.Roles(0)</td>
                        <td> <a href='#' onclick="Edit('@d.UserName')" class='btn btn-primary edit-data' rel='tooltip' title='Edit' data-placement='left' data-target="#modaledituser" data-toggle="modal"><i class='glyphicon glyphicon-pencil'></i></a>
                            <a href='#' onclick='return Delete("@d.UserName")' class='btn btn-danger btn-del' rel='tooltip' title='Delete' data-placement='right'><i class='glyphicon glyphicon-remove'></i></a>
                        </td>
                    </tr> 
                @Code Next
                    End If
                 End Code
                     
   </tbody>
</table>
<!-- Create Modal -->
<div class="modal fade" id="modaluser" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Tambah User</h4>
            </div>
            <div class="modal-body">
                @Code
                    Using Html.BeginForm("Register", "UserManagement", Nothing, FormMethod.Post, New With {.id = "formcreateuser"})
                @Html.ValidationSummary(True)
                End Code
                <div class="form-horizontal">
                    <div class="form-group">
                    <label class="col-xs-4 control-label">@Html.LabelFor(Function(m) m.UserName) :</label>
                    <div class="col-xs-4">
                         @Html.TextBoxFor(Function(m) m.UserName, New With {.class = "form-control"})
                         @Html.ValidationMessageFor(Function(model) model.UserName)
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label">@Html.LabelFor(Function(m) m.Email) :</label>
                    <div class="col-xs-4">
                       @Html.TextBoxFor(Function(m) m.Email, New With {.class = "form-control"})
                         @Html.ValidationMessageFor(Function(model) model.Email)
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label">@Html.LabelFor(Function(m) m.Password) :</label>
                    <div class="col-xs-4">
                       @Html.PasswordFor(Function(m) m.Password, New With {.class = "form-control"})
                         @Html.ValidationMessageFor(Function(model) model.Password)
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label">@Html.LabelFor(Function(m) m.ConfirmPassword) :</label>
                    <div class="col-xs-4">
                       @Html.PasswordFor(Function(m) m.ConfirmPassword, New With {.class = "form-control"})
                         @Html.ValidationMessageFor(Function(model) model.ConfirmPassword)
                    </div>
                    </div>
                     <div class="form-group">
                    <label class="col-xs-4 control-label">Role Name :</label>
                    <div class="col-xs-4">
                       <select id="Roles" name="Roles" class = "form-control">
                        @Code Dim role = Roles.GetAllRoles()
                                For Each r In role
                            End Code        
                                <option value="@r">@r</option>
                        @Code   Next
                        End Code
                       </select>
                    </div>
                    </div>
                    <center>
                   <button class="btn btn-primary" type="submit" id="btn-submit" />
                        Simpan</button>
                    &nbsp;
                    <button class="btn btn-default" type="reset" id="btn-reset">
                        Reset</button>
                    </center>

                   </div>
                @Code
                    End Using
                End Code
                 </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close</button>
            </div>
            </div>
        </div>
    </div>

<!-- Edit Modal -->
<div class="modal fade" id="modaledituser" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Ubah User</h4>
            </div>
            <div class="modal-body">
                @Code
                    Using Html.BeginForm("ChangePassword", "UserManagement", Nothing, FormMethod.Post, New With {.id = "formedituser"})
                @Html.ValidationSummary(True)
                End Code
                <div class="form-horizontal">
                    <div id ="AlertUser" style="display: none">
                       <span id="AlertUserMessage"></span>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label">@Html.LabelFor(Function(m) m.UserName) :</label>
                    <div class="col-xs-4">
                       @* @Html.TextBoxFor(Function(m) m.UserName, New With {.class = "form-control username"})
                         @Html.ValidationMessageFor(Function(model) model.UserName)*@
                         <input type="text" id="UserName" name ="UserName" value="" class = "form-control username" readonly = "readonly" />
                        
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label">@Html.LabelFor(Function(m) m.Email) :</label>
                    <div class="col-xs-4">
                       @Html.TextBoxFor(Function(m) m.Email, New With {.class = "form-control email"})
                         @Html.ValidationMessageFor(Function(model) model.Email)
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label"> @Html.LabelFor(Function(m) m.OldPassword) :</label>
                    <div class="col-xs-4">
                         @Html.PasswordFor(Function(m) m.OldPassword, New With {.class = "form-control"})
                         @Html.ValidationMessageFor(Function(m) m.OldPassword)
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label"> @Html.LabelFor(Function(m) m.NewPassword) :</label>
                    <div class="col-xs-4">
                       @Html.PasswordFor(Function(m) m.NewPassword, New With {.class = "form-control"})
                         @Html.ValidationMessageFor(Function(model) model.NewPassword)
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label">@Html.LabelFor(Function(m) m.ConfirmNewPassword) :</label>
                    <div class="col-xs-4">
                       @Html.PasswordFor(Function(m) m.ConfirmNewPassword, New With {.class = "form-control"})
                         @Html.ValidationMessageFor(Function(model) model.ConfirmNewPassword)
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-xs-4 control-label">Role Name :</label>
                    <div class="col-xs-4">
                       <select id="Roles" name="Roles" class = "form-control">
                        @Code Dim role = Roles.GetAllRoles()
                                For Each r In role
                            End Code        
                                <option value="@r">@r</option>
                        @Code   Next
                        End Code
                       </select>
                    </div>
                    </div>
                    <center>
                    <button class="btn btn-primary" type="submit" id="btn-submit" >
                        Simpan</button>
                    &nbsp;
                    <button class="btn btn-default" type="reset" id="btn-reset">
                        Reset</button>
                   
                    </center>
                    </div>
                @Code
                    End Using
                End Code
                 </div>
                 <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close</button>
            </div>
            </div>
        </div>
    </div>