﻿@ModelType NiagaAgro_ERP.LogOnModel
@Code
    ViewData("Title") = "Login"
        Layout = "~/Views/Shared/_LoginLayout.vbhtml"
End Code

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Niaga Agro</title>
    <link href="@Url.Content("~/Scripts/plugin/bootstrap/css/bootstrap.css")" rel="stylesheet" type="text/css"
        media="screen" />
    <link rel="stylesheet" href="@Url.Content("~/Content/Site.css")" type="text/css"
        media="screen" title="default" />
    <link href="@Url.Content("~/Content/validation-summary.css")" rel="stylesheet"
        type="text/css" media="screen" />
    <script src="@Url.Content("~/Scripts/jquery-1.11.2.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/plugin/bootstrap/js/bootstrap.js")" type="text/javascript"></script>
</head>
<body id="login-bg">
    <!-- Start:: login-holder -->
    <div id="login-holder">
        <!-- Start:: logo -->
        <div id="logo-login">
            @*<a href="/">
                <img src="@url.content("~/Content/Images/shared/logo.png")" width="320" height="70"
                    alt="" /></a>*@
        </div>
        <!-- End:: logo -->
        <div class="clear">
        </div>
        @Code Using Html.BeginForm(New with{.ReturnUrl = ViewBag.ReturnUrl, .class = "form-horizontal"})
                  @Html.ValidationSummary() End Code
        <!--  Start: loginbox  -->
        <div id="loginbox">
            <!--  Start: login-inner -->
            <div id="login-inner">
                <table>
                    <tr>
                        <th>
                            User Name
                        </th>
                        <td>
                            @Html.TextBoxFor(Function(m) m.UserName, New With {.class = "login-inp", .onfocus = "this.value=''"})
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Password
                        </th>
                        <td>
                            @Html.PasswordFor(Function(m) m.Password, New With {.class = "login-inp", .onfocus = "this.value=''"})
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>@Html.CheckBoxFor(Function(m) m.RememberMe)
                             @Html.LabelFor(Function(m) m.RememberMe)
                        </td>
                    </tr>
                    <tr>
                        <th>
                        </th>
                        <td>
                            <input type="submit" class="btn btn-info" value="Login" />
                        </td>
                    </tr>
                </table>
            </div>
            <!--  End: login-inner -->
            <div class="clear">
            </div>
        </div>
        <!--  End: loginbox -->
        @Code End Using End Code
    </div>
    <!-- End:: login-holder -->
    <!-- Modal -->
    <!-- start modal dialog -->
    <div id="myModal" class="modal span-3 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;</button>
            <h3>
                Login Gagal</h3>
        </div>
        <div class="modal-body">
            <p>
                @Html.ValidationSummary()</p>
        </div>
        <div class="modal-footer">
            <a href="" class="btn btn-alt">OK</a>
        </div>
    </div>
    <!-- end modal dialog -->
    <script type="text/javascript">

        $(function () {

            var errors = $(".validation-summary-errors");
            if (errors.length) {
                $("#myModal").modal();
            }

            $("#UserName").focus();

        });
 
    </script>
</body>
</html>
