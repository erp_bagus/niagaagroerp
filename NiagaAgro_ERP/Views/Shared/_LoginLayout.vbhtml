﻿<!DOCTYPE html>
<html>
<head>
    <title>@ViewData("Title") - NiagaAgro</title>
     <link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
    @*<script src="@Url.Content("~/Scripts/jquery-1.4.4.min.js")" type="text/javascript"></script>*@
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/font-awesome.min.css")" />
    @*<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/validation-summary.css")" />*@
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/bootstrap/css/bootstrap.min.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/navbar-fixed-top.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/offcanvas.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/alertifyjs/alertify.min.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/formValidation.min.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
    @RenderSection("cssfile",False)
    @*<script type="text/javascript" src="@Url.Content("~/Scripts/jquery-2.1.1.min.js")"></script>*@
    <script src="@Url.Content("~/Scripts/jquery-1.11.2.min.js")" type="text/javascript"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/bootstrap/js/bootstrap.min.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/alertifyjs/alertify.min.js")"></script>
    @RenderSection("jscript", False)
</head>
<body>
     <div class="container">
      <div class="row row-offcanvas row-offcanvas-right">
    @RenderBody()
    </div>
    </div>
</body>
</html>
