﻿<!DOCTYPE html>
<html>
<head>
    <title>@ViewData("Title") - NiagaAgro</title>
     <link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/font-awesome.min.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/validation-summary.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/bootstrap/css/bootstrap.min.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/navbar-fixed-top.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/offcanvas.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/alertifyjs/alertify.min.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Content/formValidation.min.css")" />
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
    <style type="text/css">
        .text-right
        {
            text-align:right;
        }
    </style>
    @RenderSection("cssfile",False)
    <script src="@Url.Content("~/Scripts/jquery-1.11.2.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-migrate.min.js")" type="text/javascript"></script>
  <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/bootstrap/js/bootstrap.min.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/alertifyjs/alertify.min.js")"></script>
    @RenderSection("jscript", False)
</head>
<body>
 <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><i>Niaga Agro Information System</i> </a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          <li><a href="@Url.Action("Index", "Home")">Home</a></li>
            <li><a href="@Url.Action("Index","Purchase")"><i class="glyphicon glyphicon-folder-open"></i> Pembelian</a></li>
            <li><a href="@Url.Action("Index","Sale")"><i class="glyphicon glyphicon-shopping-cart"></i>Penjualan</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Inventaris <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="@Url.Action("Index", "Customer")">Data Pelanggan</a></li>
                @Code If (Request.IsAuthenticated And User.IsInRole("Administrator")) Then End Code
                <li><a href="@Url.Action("Index", "UserManagement")">Data User</a></li>
                @Code End If End Code
                <li class="divider"></li>
                <li class="dropdown-header">Laporan</li>
                <li><a href="@Url.Action("PrintSaleRecap", "Sale")">Data Penjualan</a></li>
                <li><a href="@Url.Action("PrintSalePaid", "Sale")">Data Pelunasan Per-Pelanggan</a></li>
                <li><a href="@Url.Action("PrintAllDebt", "Sale")">Data Pelunasan</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
             @Code If (Request.IsAuthenticated And User.IsInRole("Administrator")) Then End Code
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th-large"></i>Konfigurasi <span class="caret"></span></a>
                 <ul class="dropdown-menu" role="menu">
                 <li><a href="@Url.Action("Index","Material")">Material</a></li>
                  <li><a href="@Url.Action("Index","Unit")">Unit</a></li>
                  @*<li><a href="@Url.Action("Index","Type")">Tipe</a></li>*@
                 </ul>
            </li>
             @Code End If End Code   
            <!--li><a href="">Static top</a></li>
            <li class="active"><a href="#">Fixed top</a></li-->
            <li style="margin-top:15px">@Code Using Html.BeginForm("LogOff", "Account", FormMethod.Post, New With {.id = "logoutForm"})End Code
                 @Html.AntiForgeryToken()
                <a data-toggle="tooltip" data-placement="bottom" href="javascript:document.getElementById('logoutForm').submit()" title='Logout, @User.Identity.Name' class="btn btn-default"><i class="glyphicon glyphicon-off"></i>Log off</a>
                @Code End Using End Code 
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
     <div class="container">
      <div class="row row-offcanvas row-offcanvas-right">
    @RenderBody()
    </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
     $('[data-toggle="tooltip"]').tooltip()
     $('.nav li').each(function(){
        var href = $(this).find('a').attr('href');
        if (href === window.location.pathname) {
            $(this).addClass('active');
        }
     });
});
</script>
</html>
