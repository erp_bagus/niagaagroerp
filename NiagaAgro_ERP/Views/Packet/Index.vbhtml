﻿@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i><i class="glyphicon glyphicon-tasks"></i>
    <strong>Informasi Paket</strong>
</div>
<a class="btn btn-default btn-add" href="@Url.Action("Create","Sale")">
    <i class="glyphicon glyphicon-plus"></i>
</a>
<hr />
<table class="table table-bordered" id="tblsale">
    <thead>
        <tr class="success">
            <th>
                Nama Barang
            </th>
            <th>
                Tanggal
            </th>
            <th>
                Jumlah
            </th>
            <th>
                Satuan
            </th>
            <th>
                Tipe
            </th>
            <th>
                Keterangan
            </th>
            <th>
            </th>
        </tr>
    </thead>
</table>
