﻿@ModelType NiagaAgro_ERP.ModelPurchase
    
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@Imports NiagaAgro_ERP.HtmlHelpers
<div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i> <i class="glyphicon glyphicon-tasks"></i>
    <strong>Informasi Pembelian</strong>
</div>
<button class="btn btn-default btn-add" data-target="#modalpurchase" data-toggle="modal">
    <i class="glyphicon glyphicon-plus"></i>
</button>
<hr />
<table class="table table-bordered" id="tblPurchase">
    <thead>
        <tr class="success">
            <th>
                Nama Barang
            </th>
            <th>
                Tanggal Masuk
            </th>
            <th>
                Jumlah
            </th>
            <th>
                Modal
            </th>
            <th>
                Jenis Barang
            </th>
            <th>
            </th>
        </tr>
    </thead>
</table>
<!-- Modal -->
<div class="modal fade" id="modalpurchase" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Tambah Barang</h4>
            </div>
            <div class="modal-body">
                @Using Html.BeginForm("Create", "Purchase", Nothing, FormMethod.Post, New With {.class = "form-horizontal", .id = "form-purchase"})
                    @Html.ValidationSummary(True)
                     @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Nama Barang
                        </label>
                        <div class="col-sm-5">
                            @Html.DropDownList("IDBarang", New SelectList(DirectCast(ViewData("Barang"), IEnumerable), "ID", "NamaBrg"), "Barang", New With{.class="form-control"})
                           @Html.ValidationMessageFor(Function(model) model.IDBarang)
                        </div>
                    </div>
                        @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Tanggal
                        </label>
                        <div class="col-sm-5">
                         <div class="input-group">
                            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                            @Html.TextBoxFor(Function(model) model.Tanggal, New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.Tanggal)
                            </div>
                        </div>
                    </div>
                    
                        @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Jumlah
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBoxFor(Function(model) model.Jumlah, New With{.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.Jumlah)
                        </div>
                    </div>
                        
                        @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Modal
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBox("Modal", 0, New With {.class = "form-control"})
                             @Html.ValidationMessageFor(Function(model) model.Modal)
                        </div>
                    </div>
                     @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Distributor
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBox("Distributor", Nothing, New With {.class = "form-control"})
                             @Html.ValidationMessageFor(Function(model) model.Distributor)
                        </div>
                    </div>
                     @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Nomor PO
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBox("NomorPO", Nothing, New With {.class = "form-control"})
                             @Html.ValidationMessageFor(Function(model) model.NomorPO)
                        </div>
                    </div>
                      @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Tanggal PO
                        </label>
                        <div class="col-sm-5">
                         <div class="input-group">
                            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                            @Html.TextBoxFor(Function(model) model.TanggalPO, New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.TanggalPO)
                            </div>
                        </div>
                    </div>
                        @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Keterangan
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBox("Keterangan", Nothing, New With {.class = "form-control"})
                             @Html.ValidationMessageFor(Function(model) model.Keterangan)
                        </div>
                    </div>
                        @<center>
                        <button class="btn btn-primary" type="submit" id="btn-submit">
                            Save</button>
                        &nbsp;
                        <button class="btn btn-default" type="reset" id="btn-reset">
                            Reset</button>
                    </center>
                End Using
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close</button>
            </div>
        </div>
    </div>
</div>

@*@Using Html.LoadingHelpers(Url.Content("~/Content/img/progress.gif"))
End Using*@
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
  <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment.min.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/moment-with-locales.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/moment/id.js")"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DatetimePicker/bootstrap-datetimepicker.js")"></script>
     <script src="@Url.Content("~/Content/select2-3.3.2/select2.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/autonumeric/autoNumeric.js")"></script>
    <script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/Module/Purchase/index.js")"></script>
End Section
