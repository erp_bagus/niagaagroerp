﻿@ModelType NiagaAgro_ERP.MasterBarang
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@Imports NiagaAgro_ERP.HtmlHelpers
<div class="alert alert-info">
    <i class="glyphicon glyphicon-list"></i>&nbsp; <strong>Informasi Data Material</strong>
</div>
<button class="btn btn-default btn-add" data-target="#modalmaterial" data-toggle="modal"
    rel="tooltip" title="Create" data-placement="left">
    <i class="glyphicon glyphicon-plus"></i>
</button>
<hr />
<table class="table table-bordered" id="tblbrg">
    <thead>
        <tr class="success">
            <th>
                Nama Barang
            </th>
            <th>
                Kemasan
            </th>
            <th>
                Satuan
            </th>
             <th>
                Jenis Barang
            </th>
            <th>
            </th>
        </tr>
    </thead>
</table>
<!-- Modal -->
<div class="modal fade" id="modalmaterial" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Tambah Barang</h4>
            </div>
            <div class="modal-body">
                @Using Html.BeginForm("Create", "Material", Nothing, FormMethod.Post, New With {.class = "form-horizontal", .id = "form-material"})
                    @Html.ValidationSummary(True)
                    @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Nama Barang
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBox("NamaBrg", Nothing, New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.NamaBrg)
                        </div>
                    </div>
                     @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Jenis Barang
                        </label>
                        <div class="col-sm-5">
                            @Html.DropDownList("JenisBarang", New SelectList(ViewData("jenisbrg")),New With{.class="form-control"} )
                            @Html.ValidationMessageFor(Function(model) model.JenisBarang)
                        </div>
                    </div>
                    @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Kemasan
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBox("Kemasan", Nothing, New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.Kemasan)
                        </div>
                    </div>
                    @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Unit
                        </label>
                        <div class="col-sm-5">
                            @Html.DropDownList("IDUnit", New SelectList(DirectCast(ViewData("Unit"), IEnumerable), "ID", "Satuan"), "", New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.IDUnit)
                        </div>
                    </div>
                    @<center>
                        <button class="btn btn-primary" type="submit" id="btn-submit">
                            Save</button>
                        &nbsp;
                        <button class="btn btn-default" type="reset" id="btn-reset">
                            Reset</button>
                    </center>
                End Using
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close</button>
            </div>
        </div>
    </div>
</div>
@Using Html.LoadingHelpers(Url.Content("~/Content/img/progress.gif"))
End Using

@section cssFile
    <link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
End Section
@section jscript
    <script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
    <script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
    <script type="text/javascript" src="@Url.Content("~/Scripts/Module/Material/index.js")"></script>
End Section
