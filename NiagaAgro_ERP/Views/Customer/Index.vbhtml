﻿@ModelType NiagaAgro_ERP.ModelCustomer
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@Imports NiagaAgro_ERP.HtmlHelpers
<div class="alert alert-info">
    <i class="glyphicon glyphicon-log-in"></i> <i class="glyphicon glyphicon-tasks"></i>
    <strong>Informasi Customer</strong>
</div>
<button class="btn btn-default btn-add" data-target="#modalcustomer" data-toggle="modal">
    <i class="glyphicon glyphicon-plus"></i>
</button>
<hr />
<table class="table table-bordered" id="tblCustomer">
    <thead>
        <tr class="success">
            <th>
                Nama
            </th>
            <th>
                Jenis Kelamin
            </th>
            <th>
                Alamat
            </th>
            <th>
                No HP/Telepone
            </th>
            <th>
            </th>
        </tr>
    </thead>
</table>
<!-- Modal -->
<div class="modal fade" id="modalcustomer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Tambah Customer</h4>
            </div>
            <div class="modal-body">
                @Using Html.BeginForm("Save", "Customer", Nothing, FormMethod.Post, New With {.class = "form-horizontal", .id = "form-customer"})
                    @Html.ValidationSummary(True)
                     @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Nama
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBoxFor(Function(m) m.Nama, New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.Nama)
                        </div>
                    </div>
                        @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Alamat
                        </label>
                        <div class="col-sm-5">
                            @Html.TextBoxFor(Function(m) m.Alamat, New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.Alamat)
                            
                        </div>
                    </div>
                     @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Jenis Kelamin
                        </label>
                        <div class="col-sm-5">
                             @Html.DropDownList("JenisKelamin")
                        </div>
                    </div>
                        @<div class="form-group">
                        <label class="col-sm-3 control-label">
                            No HP/Telepon
                        </label>
                        <div class="col-sm-5">
                           @Html.TextBoxFor(Function(m) m.Phone, New With {.class = "form-control"})
                            @Html.ValidationMessageFor(Function(model) model.Phone)
                        </div>
                    </div>
                        
                       
                    @<center>
                        <button class="btn btn-primary" type="submit" id="btn-submit">
                            Save</button>
                        &nbsp;
                        <button class="btn btn-default" type="reset" id="btn-reset">
                            Reset</button>
                    </center>
                End Using
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close</button>
            </div>
        </div>
    </div>
</div>
@Using Html.LoadingHelpers(Url.Content("~/Content/img/progress.gif"))
End Using
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
<script src="@Url.Content("~/Scripts/Module/general/general.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Content/select2-3.3.2/select2.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Module/Customer/Index.js")" type="text/javascript"></script>
End Section