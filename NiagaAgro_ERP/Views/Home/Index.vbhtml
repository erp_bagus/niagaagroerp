﻿@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="alert alert-info">
    <i class="glyphicon glyphicon-list-alt"></i> <strong>Informasi Inventaris Barang</strong>
</div>
<hr />
<table class="table table-bordered" id="tblstok">
    <thead>
        <tr class="success">
            <th>
                Nama Barang
            </th>
           
            <th>
                Jumlah Barang Masuk
            </th>
            <th>
                  Jumlah Barang Keluar
            <th>
               Stok
            </th>
          
        </tr>
    </thead>
</table>
@section cssFile
<link rel="Stylesheet" type="text/css" href="@Url.Content("~/Scripts/plugin/DataTables/css/jquery.dataTables.min.css")" />
End Section
@section jscript
<script type="text/javascript" src="@Url.Content("~/Scripts/plugin/DataTables/js/jquery.dataTables.min.js")"></script>
 <script type="text/javascript" src="@Url.Content("~/Scripts/Module/Home/index.js")"></script>
End Section