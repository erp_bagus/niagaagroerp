﻿Public Class BaseRepository

    Private _entity As NiagaAgroEntities
    Public Sub New()
        _entity = New NiagaAgroEntities
    End Sub
    Public ReadOnly Property Entities As NiagaAgroEntities
        Get
            Return _entity
        End Get
    End Property
    Protected Friend Function getCurrentCulture() As System.Globalization.CultureInfo
        Return New System.Globalization.CultureInfo("ID-id")
    End Function
    Public Property ErrorMessage As String
End Class
Public Class StockBarang
    Public Property IDBarang As Integer
    Public Property JenisBarang As String
    Public Property NamaBrg As String
    Public Property JlhMasuk As Integer
    Public Property JlhKeluar As Integer
    Public Property stock As Integer
End Class
