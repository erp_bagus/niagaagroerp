﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization

Public Class UserViewModel
    Private userNameValue As String
    Private passwordValue As String
    Private confirmPasswordValue As String
    Private emailValue As String
    Private oldPasswordValue As String
    Private newPasswordValue As String
    Public Property Roles As String()
    Public Property Role As String

    <Required()> _
    <Remote("checkDuplicateName", "UserManagement")> _
    <Display(Name:="User name")> _
    Public Property UserName() As String
        Get
            Return userNameValue
        End Get
        Set(ByVal value As String)
            userNameValue = value
        End Set
    End Property

    <Required()> _
    <DataType(DataType.EmailAddress)> _
    <Display(Name:="Email address")> _
    Public Property Email() As String
        Get
            Return emailValue
        End Get
        Set(ByVal value As String)
            emailValue = value
        End Set
    End Property

    <Required()> _
    <StringLength(100, ErrorMessage:="The {0} must be at least {2} characters long.", MinimumLength:=6)> _
    <DataType(DataType.Password)> _
    <Display(Name:="Password")> _
    Public Property Password() As String
        Get
            Return passwordValue
        End Get
        Set(ByVal value As String)
            passwordValue = value
        End Set
    End Property

    <DataType(DataType.Password)> _
    <Display(Name:="Confirm password")> _
    <Compare("Password", ErrorMessage:="The password and confirmation password do not match.")> _
    Public Property ConfirmPassword() As String
        Get
            Return confirmPasswordValue
        End Get
        Set(ByVal value As String)
            confirmPasswordValue = value
        End Set
    End Property

    <Required()> _
    <DataType(DataType.Password)> _
    <Display(Name:="Current password")> _
    Public Property OldPassword() As String
        Get
            Return oldPasswordValue
        End Get
        Set(ByVal value As String)
            oldPasswordValue = value
        End Set
    End Property

    <Required()> _
    <StringLength(100, ErrorMessage:="The {0} must be at least {2} characters long.", MinimumLength:=6)> _
    <DataType(DataType.Password)> _
    <Display(Name:="New password")> _
    Public Property NewPassword() As String
        Get
            Return newPasswordValue
        End Get
        Set(ByVal value As String)
            newPasswordValue = value
        End Set
    End Property

    <DataType(DataType.Password)> _
    <Display(Name:="Confirm new password")> _
    <Compare("NewPassword", ErrorMessage:="The new password and confirmation password do not match.")> _
    Public Property ConfirmNewPassword() As String
        Get
            Return confirmPasswordValue
        End Get
        Set(ByVal value As String)
            confirmPasswordValue = value
        End Set
    End Property
End Class

Public Class LogOnModel
    Private userNameValue As String
    Private passwordValue As String
    Private rememberMeValue As Boolean

    <Required()> _
    <Remote("CheckDuplicate", "UserManagement")> _
    <Display(Name:="User name")> _
    Public Property UserName() As String
        Get
            Return userNameValue
        End Get
        Set(ByVal value As String)
            userNameValue = value
        End Set
    End Property

    <Required()> _
    <DataType(DataType.Password)> _
    <Display(Name:="Password")> _
    Public Property Password() As String
        Get
            Return passwordValue
        End Get
        Set(ByVal value As String)
            passwordValue = value
        End Set
    End Property

    <Display(Name:="Remember me?")> _
    Public Property RememberMe() As Boolean
        Get
            Return rememberMeValue
        End Get
        Set(ByVal value As Boolean)
            rememberMeValue = value
        End Set
    End Property
End Class

Interface IUserManagementModel

    Function GetUserById(UserId As String) As UserViewModel
    Function GetUserList() As List(Of UserViewModel)
End Interface

Public Class UserManagementRepository
    Inherits BaseRepository : Implements IUserManagementModel

    Public Function GetUserList() As List(Of UserViewModel) Implements IUserManagementModel.GetUserList
        Dim users = Membership.GetAllUsers()
        Dim listmodel As New List(Of UserViewModel)
        For Each u As MembershipUser In users
            Dim newuser As New UserViewModel
            newuser.UserName = u.UserName
            newuser.Password = String.Empty
            newuser.Email = u.Email
            newuser.Roles = Roles.GetRolesForUser(u.UserName)
            listmodel.Add(newuser)
        Next
        Return listmodel
    End Function
    Public Function GetUserById(ByVal UserName As String) As UserViewModel Implements IUserManagementModel.GetUserById
        Dim model = GetUserList().Where(Function(a) a.UserName.Trim = UserName.Trim).FirstOrDefault
        Return model
    End Function
    Public Sub DeleteUser(ByVal UserId As Guid)
        Dim ctx As New NiagaAgroEntities
        ctx.ExecuteStoreCommand("DELETE FROM UserRegister WHERE UserId=@UserId", New SqlClient.SqlParameter("@UserId", UserId))
        ctx.ExecuteStoreCommand("DELETE FROM aspnet_PersonalizationPerUser WHERE UserId=@UserId", New SqlClient.SqlParameter("@UserId", UserId))
        ctx.ExecuteStoreCommand("DELETE FROM aspnet_Profile WHERE UserId=@UserId", New SqlClient.SqlParameter("@UserId", UserId))
        ctx.ExecuteStoreCommand("DELETE FROM aspnet_UsersInRoles WHERE UserId=@UserId", New SqlClient.SqlParameter("@UserId", UserId))
        ctx.ExecuteStoreCommand("DELETE FROM aspnet_Membership WHERE UserId=@UserId", New SqlClient.SqlParameter("@UserId", UserId))
        ctx.ExecuteStoreCommand("DELETE FROM aspnet_Users WHERE UserId=@UserId", New SqlClient.SqlParameter("@UserId", UserId))
    End Sub
End Class