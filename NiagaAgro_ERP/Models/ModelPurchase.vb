﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization
<MetadataType(GetType(ModelPurchase))>
Public Class TblBrgMasuk
    Protected ctx As New NiagaAgroEntities
    Public Sub Save()
        ctx.TblBrgMasuks.AddObject(Me)
        If Me.ID > 0 Then
            ctx.ObjectStateManager.ChangeObjectState(Me, Data.EntityState.Modified)
        End If
        ctx.SaveChanges()
    End Sub
End Class
Public Class ModelPurchase
    <Required(ErrorMessage:="ID harus diisi.")>
    Public Property ID() As Integer
    <Required(ErrorMessage:="Nama Barang harus diisi.")>
    Public Property IDBarang() As Integer
    <Required(ErrorMessage:="Tanggal harus diisi.")>
    Public Property Tanggal() As DateTime
    <Required(ErrorMessage:="Tanggal PO harus diisi.")>
    Public Property TanggalPO() As DateTime
    <Required(ErrorMessage:="Jumlah harus diisi.")>
    Public Property Jumlah() As Integer
    <Required(ErrorMessage:="Modal harus diisi.")>
    Public Property Modal() As Integer
    <Required(ErrorMessage:="Distributor harus diisi.")>
    Public Property Distributor() As String
    <Required(ErrorMessage:="Nomor PO harus diisi.")>
    Public Property NomorPO() As String
    <Required(ErrorMessage:="Keterangan harus diisi.")>
    Public Property Keterangan() As String
End Class