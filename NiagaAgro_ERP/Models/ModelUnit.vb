﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization
<MetadataType(GetType(ModelUnit))>
Public Class TblSatuanUnit
    Protected ctx As New NiagaAgroEntities
    Public Sub Save()
        ctx.TblSatuanUnits.AddObject(Me)
        If Me.ID > 0 Then
            ctx.ObjectStateManager.ChangeObjectState(Me, Data.EntityState.Modified)
        End If
        ctx.SaveChanges()
    End Sub
End Class
Public Class ModelUnit
    <Required(ErrorMessage:="Satuan harus diisi.")>
    Public Property Satuan() As String
    Public Property Keterangan() As String
End Class
