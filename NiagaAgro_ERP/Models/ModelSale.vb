﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization
<MetadataType(GetType(ModelSale))>
Public Class TblPenjualan
    Protected ctx As New NiagaAgroEntities
    Public Sub Save()
        ctx.TblPenjualans.AddObject(Me)
        If Me.ID > 0 Then
            ctx.ObjectStateManager.ChangeObjectState(Me, Data.EntityState.Modified)
        End If
        ctx.SaveChanges()
    End Sub
End Class
Public Class ModelSale
    Public Property ID As Integer
    Public Property IDCustomer As Integer?
    Public Property FormNumber As String
    Public Property Tanggal As Date
    Public Property Type As Integer
    'Public Property PenjualanDetail As List(Of ModelDetailSale)
End Class
Public Class ModelPaid
    <Required(ErrorMessage:="ID harus diisi.")>
    Public Property ID As Integer

    <Required(ErrorMessage:="ID harus diisi.")>
    Public Property IDPenjualan As Integer

    <Required(ErrorMessage:="Tanggal harus diisi.")>
    Public Property Tanggal As Date

    <Required(ErrorMessage:="Bayar harus diisi.")>
    Public Property Bayar As Decimal

    <Required(ErrorMessage:="Sisa harus diisi.")>
    Public Property Sisa As Decimal
End Class

