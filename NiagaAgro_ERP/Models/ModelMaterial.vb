﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization
<MetadataType(GetType(ModelMaterial))>
Public Class MasterBarang
    Protected ctx As New NiagaAgroEntities
    Public Sub Save()
        ctx.MasterBarangs.AddObject(Me)
        If Me.ID > 0 Then
            ctx.ObjectStateManager.ChangeObjectState(Me, Data.EntityState.Modified)
        End If
        ctx.SaveChanges()
    End Sub
End Class
Public Class ModelMaterial
    <Required(ErrorMessage:="Jenis barang harus diisi.")>
    Public Property JenisBarang() As String
    <Required(ErrorMessage:="Nama barang harus diisi.")>
    Public Property NamaBrg() As String

    <Required(ErrorMessage:="Kemasan harus diisi.")>
    Public Property Kemasan() As String

    <Required(ErrorMessage:="Unit harus diisi.")>
    Public Property IDUnit() As Integer
End Class
