﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization
Public Class ModelDetailSale
    Public Property IDPenjualan As Integer
    Public Property IDBarang As Integer
    Public Property _Tanggal As Date
    Public Property Jumlah As Integer
    Public Property HargaJual As Decimal
    Public Property Keterangan As String = Nothing
    Public Property U1 As Decimal = Nothing
    Public Property U2 As Decimal = Nothing
    Public Property Nama As String = Nothing
End Class
