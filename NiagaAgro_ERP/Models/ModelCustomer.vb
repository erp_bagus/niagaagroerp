﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization
<MetadataType(GetType(ModelCustomer))>
Public Class TblCustomer
    Protected ctx As New NiagaAgroEntities
    Public Sub Save()
        ctx.TblCustomers.AddObject(Me)
        If Me.ID > 0 Then
            ctx.ObjectStateManager.ChangeObjectState(Me, Data.EntityState.Modified)
        End If
        ctx.SaveChanges()
    End Sub
End Class
Public Class ModelCustomer
    <Required(ErrorMessage:="ID harus diisi.")>
    Public Property ID As Integer
    <Required(ErrorMessage:="Nama harus diisi.")>
    Public Property Nama As String

    <Required(ErrorMessage:="Alamat harus diisi.")>
    Public Property Alamat As String

    <Required(ErrorMessage:="No HP/Telepon harus diisi.")>
    Public Property Phone As String
    Public Property JenisKelamin As String
End Class
