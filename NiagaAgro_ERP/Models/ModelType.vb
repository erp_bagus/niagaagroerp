﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization
<MetadataType(GetType(ModelType))>
Public Class TblType
    Protected ctx As New NiagaAgroEntities
    Public Sub Save()
        ctx.TblTypes.AddObject(Me)
        If Me.ID > 0 Then
            ctx.ObjectStateManager.ChangeObjectState(Me, Data.EntityState.Modified)
        End If
        ctx.SaveChanges()
    End Sub
End Class
Public Class ModelType
    <Required(ErrorMessage:="field is required.")>
    Public Property Id() As Integer
    <Required(ErrorMessage:="field is required.")>
    Public Property Type() As String
End Class
